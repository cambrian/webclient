#!/usr/bin/perl

#WIP
# This script generates a list of package names.
# This should be called at the top level. (say, ..src\com\brio.)
#WIP

foreach $dir (<*>) {
    if(-d $dir) {
        readPKGName($dir);
    }
}

sub readPKGName {
    my ($bChecked);
    $bChecked = 0;
    # Read it from the first java file (not from the directory name)....
    foreach $file (<$_[0]\\*>) {
        $i_ext = index($file,".java");
        if($bChecked==0 && $i_ext>0) {
            #open(OLDFILE, $file) || die "Sorry, I couldn't open $file\n";
            if(open(OLDFILE, $file)) {
                while(<OLDFILE>) {
                    $i_pkg = index($_,"package");
                    $i_com = index($_,"com.");
                    if($i_pkg>=0 && $i_com>=0) {
                        $i_end = index($_,";");
                        $length = $i_end - $i_com;
                        $pkgName = substr($_,$i_com,$length);
                        print $pkgName . "\n";
                        # We've seen one, the rest are the same.
                        $bChecked = 1;
                        break;
                    }
                }
                close(OLDFILE);
            }
        } elsif(-d $file) {
            readPKGName($file);
        }
    }
}
