#!/usr/bin/perl

# [1] Use globbing
#  foreach $file (<*.java>) {
#      replaceString($file);
#  }

#  # [2] Multiple files can be given as input
#  for($i=0;$i<@ARGV;$i++) {
#      replaceString($ARGV[$i]);
#  }

# [3] All files under the given directory
foreach $dir (<*>) {
    if(-d $dir) {
        replaceStringRecursively($dir);
    }
}

sub replaceString {
    $tmpfile = $_[0] . "_tmp";
    rename($_[0],$tmpfile);
    $newfile = $_[0];

    open(HLFILE, $tmpfile) || die "Sorry, I couldn't open $tmpfile\n";
    open(HFILE, ">".$newfile) || die "Sorry, I couldn't open $newfile\n";
    while(<HLFILE>) {
        s/BlueCraft, Inc\./BlueCraft Software\./g;
        print HFILE $_;
    }
    close(HFILE);
    close(HLFILE);
    unlink($tmpfile);
}


sub replaceStringRecursively {
    foreach $file (<$_[0]\\*>) {
        $i_ext = index($file,".java");
        $j_ext = rindex($file,".java");
        if($i_ext>0 && $i_ext==$j_ext) {
            replaceString($file);
        } elsif(-d $file) {
            replaceStringRecursively($file);
        }
    }
}

#  sub replaceStringRecursively {
#      foreach $file (<$_[0]\\*>) {
#          if(-d $file) {
#              replaceStringRecursively($file);
#          } else {
#              replaceString($file);
#          }
#      }
#  }

