#!/usr/bin/perl

#WIP


foreach $file (<*.java>) {
    $srcDir = "..\\src\\";

    open(OLDFILE, $file) || die "Sorry, I couldn't open $file\n";
    while(<OLDFILE>) {
        $i_pkg = index($_,"package");
        $i_com = index($_,"com.bluecraft");
        if($i_pkg>=0 && $i_com>=0) {
            $i_end = index($_,";");
            $length = $i_end - $i_com;
            $pkgName = substr($_,$i_com,$length);
            $pkgName =~ tr/./\\/;
            $newDir = $srcDir . $pkgName;
            mkdir($newDir,0777); # ignore if it fails
            $newfile = $srcDir . $pkgName . "\\" . $file;
            print $newfile . "\n";
            break;
        }
    }
    close(OLDFILE);

    rename($file, $newfile); # || die "...: $!";
}

