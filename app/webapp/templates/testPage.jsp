<%@ page buffer="16kb" autoFlush="true" %>
<%@ page errorPage="error.jsp" %>
<%@ page import="com.bluecraft.webclient.*,com.bluecraft.webclient.page.*" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML lang="en">
<HEAD>
<TITLE>WEB Client | Test</TITLE>
<LINK rel="STYLESHEET" href="<%=request.getContextPath()%>/styles/webclient.css" type="text/css">
</HEAD>
<BODY class="body">

<%
if(request instanceof IRequest) {
out.println("request is instanceof IRequest" + "<BR>");
} else {
out.println("request is NOT instanceof IRequest" + "<BR>");
}
%>

<%
TestRForm tRF1 = (TestRForm) request.getAttribute("testRForm");
TestSForm tSF1 = (TestSForm) session.getAttribute("testSForm");
out.println("TestRForm (request) = " + tRF1 + "<BR>");
out.println("TestSForm (session) = " + tSF1 + "<BR>");
%>
<jsp:useBean id="testRForm" class="com.bluecraft.webclient.page.TestRForm" scope="request" />
<jsp:useBean id="testSForm" class="com.bluecraft.webclient.page.TestSForm" scope="session" />
<%
TestRForm tRF2 = (TestRForm) request.getAttribute("testRForm");
TestSForm tSF2 = (TestSForm) session.getAttribute("testSForm");
out.println("TestRForm (request) = " + tRF2 + "<BR>");
out.println("TestSForm (session) = " + tSF2 + "<BR>");
%>
<%--
<% testRForm.setFormName("testRForm"); %>
<% testSForm.setFormName("testSForm"); %>
--%>


<!-- Begin: Body -->
<DIV align="center">
<TABLE class="body" border="0" cellPadding="0" cellSpacing="0" width="100%">
  <TBODY>
  <TR>

    <!-- Begin: Main Part  -->
    <TD vAlign="top" width="100%">

      <TABLE border="0" cellPadding="0" cellSpacing="0" width="100%">
        <TBODY>
        <TR><TD>

          <TABLE class="plainbox" cellPadding="0" cellSpacing="0">
            <TR class="header">
              <TD noWrap>Test Form</TD>
            </TR>
            <TR><TD width="*">
                <TABLE border=0 cellPadding=0 cellSpacing=0 width="100%">
                  <TBODY>
                  <TR><TD height="5"></TD></TR>
                  </TBODY>
                </TABLE>
            </TD></TR>
            <TR>
              <TD> RForm Test
            </TD></TR>
            <TR>
              <TD>
<%--                 <FORM action="<%=testRForm.getActionHRef()%>" name="rForm" method="post"> --%>
                 <FORM action="<%=testRForm.getActionAnchor().getAbsoluteHRef(request)%>" name="rForm" method="post">
                 TestField:<BR>
                 <INPUT type="text" name="testFieldValue" value="<%=testRForm.getTestFieldValue()%>"><BR><BR>
                 <INPUT type="submit" name="Form_Submit1" value="Submit1" title="title1">
                 <INPUT type="submit" name="Form_Submit2" value="Submit2" title="title2">
                 <BUTTON type="submit" name="Form_Submit3" title="title3">Button Button</BUTTON>
                 <BUTTON type="reset" name="Form_Submit4" title="title4">Reset</BUTTON>
                 <INPUT type="hidden" name="form_id" value="<%=testRForm.getID()%>">
                 <INPUT type="hidden" name="formName" value="testRForm">
                 </FORM>
              </TD>
            </TR>
            <TR>
              <TD> &nbsp;
            </TD></TR>
            <TR>
              <TD> SForm Test
            </TD></TR>
            <TR>
              <TD>
                 <FORM action="<%=request.getContextPath()%>/appservlet/templates/about.jsp" name="sForm" method="post">
                 TestField:<BR>
                 <INPUT type="text" name="testFieldValue" value="<%=testSForm.getTestFieldValue()%>"><BR><BR>
                 <INPUT type="submit" name="Form_Submit1" value="Submit1" title="title1">
                 <INPUT type="submit" name="Form_Submit2" value="Submit2" title="title2">
                 <BUTTON type="submit" name="Form_Submit3" title="title3">Button Button</BUTTON>
                 <BUTTON type="reset" name="Form_Submit4" title="title4">Reset</BUTTON>
                 <INPUT type="hidden" name="form_id" value="<%=testSForm.getID()%>">
                 <INPUT type="hidden" name="formName" value="testSForm">
                 </FORM>
              </TD>
            </TR>
          </TABLE>

        </TD></TR>
        </TBODY>
      </TABLE>

    </TD>
    <!-- End: Main Part -->


  </TR>
</TBODY>
</TABLE>
</DIV>
<!-- End: Body -->


</BODY>
</HTML>

<!--
  Copyright (c) 2002, BlueCraft Software.  All rights reserved.
  Requested URI: <%= request.getRequestURI() %>
  $Id: testPage.jsp,v 1.2 2003/01/05 10:08:02 hyoon Exp $
-->



