/*************************************************************************
 *  Copyright (c) 2002-2003, BlueCraft Software. 
 *  All Rights Reserved. No part of this program may be copied
 *  or used without the express written consent of BlueCraft Software.
 *************************************************************************/
// $Id: DefaultProcessorManager.java,v 1.2 2003/02/02 23:12:10 hyoon Exp $ 

package com.bluecraft.webclient.processor;

import com.bluecraft.webclient.*;
import com.bluecraft.webclient.config.*;

import java.util.logging.Logger;


/**   
 *   
 * @author   <A href="mailto:hyoon@bluecraft.com">Hyoungsoo Yoon</A>
 * @version  $Revision: 1.2 $
 */   
public class DefaultProcessorManager
    implements IProcessorManager
{
    private static IProcessorManager s_processorManager = null;
    private boolean mInitialized = false;
    private IProcessor  mProcessor = null;
    
    protected DefaultProcessorManager()
    {
        //
    }

    public static IProcessorManager getInstance()
    {
        if(s_processorManager == null) {
            s_processorManager = new DefaultProcessorManager();
        }
        return s_processorManager;
    }

    protected void initialize(IConfig config)
    {
        if(config != null) {
            //
        } else {
            //
        }

        mInitialized = true;
    }

    public IProcessor getDefaultProcessor()
    {
        if (mProcessor == null) {
            // ???
            return new BaseProcessor();
        } else {
            return mProcessor;
        }
    }
    
    public IProcessor getProcessor(String className)
    {
        if (mProcessor == null) {
            try {
                Class clazz = Class.forName(className);
                mProcessor = (IProcessor) clazz.newInstance();
                //mProcessor.init(servlet, this);
            } catch (Throwable t) {
                //throw new UnavailableException("Cannot initialize IProcessor of class " + className + ": " + t);
            }
        }
        return mProcessor;
    }

    
}



