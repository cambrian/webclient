/*************************************************************************
 *  Copyright (c) 2002-2003, BlueCraft Software. 
 *  All Rights Reserved. No part of this program may be copied
 *  or used without the express written consent of BlueCraft Software.
 *************************************************************************/
// $Id: BaseProcessor.java,v 1.2 2003/02/02 23:12:10 hyoon Exp $ 

package com.bluecraft.webclient.processor;

import com.bluecraft.webclient.*;
import com.bluecraft.webclient.action.*;
import com.bluecraft.webclient.parser.*;
import com.bluecraft.webclient.client.*;
import com.bluecraft.webclient.page.*;
import com.bluecraft.webclient.util.*;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.*;
import java.io.IOException;


/**   
 *   
 * @author   <A href="mailto:hyoon@bluecraft.com">Hyoungsoo Yoon</A>
 * @version  $Revision: 1.2 $
 */   
public class BaseProcessor
    implements IProcessor
{
    private IActionManager mActionManager = null;

    public BaseProcessor()
    {
        mActionManager = DefaultActionManager.getInstance();
    }

    public IActionManager getActionManager()
    {
        return mActionManager;
    }
    
    
    //
    public final void process(HttpServletRequest request, HttpServletResponse response)
        throws IOException, ServletException
    {
        //IRequest req = new BaseRequest(request);
        //IResponse res = new BaseResponse(response);
        
        //assert (request instanceof BaseRequest);
        //assert (response instanceof BaseResponse);  
        
        try {
            IRequest req = (IRequest) request;
            IResponse res = (IResponse) response;

            String protocol = req.getProtocol();
            if("HTTP/1.1".equals(protocol)) {
                process11(req, res);
            } else {
                // ....
                LogUtil.getLogger().severe("Protocol is not HTTP/1.1");
                //
                throw new ServletException("Protocol Not Supported"); // ????
            }
        } catch(ClassCastException ccx) {
            // ....
            LogUtil.getLogger().severe("Either request is not an instance of IRequest or response is not an instance of IResponse.");
            //
            throw new ServletException(ccx); // ????
        }
    }

    
    //
    private void process11(IRequest req, IResponse res)
        throws IOException, ServletException
    {
        LogUtil.getLogger().entering("BaseProcessor","process11", new Object[]{req,res});

        // process path / url / .....
        String reqURI = req.getRequestURI();
        LogUtil.getLogger().fine("reqURI=" + reqURI);

        // process headers
        processHeaders(req, res);

        // process cookies....
        processCookies(req, res);

        // process params....
        processParameters(req, res);

        
        // session???
        //ISession sess = req.getSession();
        HttpSession sess = req.getSession();
        LogUtil.getLogger().fine("session = " + sess);
        

        // access control
        // TODO: get it from session..... ???
        //IClient clientelle = DefaultClientManager.getInstance().getClient(sess);
        IClient clientelle = DefaultClientManager.getInstance().getClient(new BaseSession(sess));
        LogUtil.getLogger().fine("client = " + clientelle);


        // process form
        boolean bCommitted = processForm(req, res);


        // ???
        //processURIPath(req, res);
        // ???
        
        
        if(!bCommitted) {
            // action mapping....
            String reqPath = req.getPathInfo();
            LogUtil.getLogger().fine("reqPath=" + reqPath);
            IAction act = mActionManager.getAction(reqPath); // session, client???
            LogUtil.getLogger().fine("action=" + act);
            if(act != null) {
                IActionRouter actRouter = act.perform(req, res); // ????
                if(actRouter != null) {
                    actRouter.route(req, res); // ????
                }
            }

            // forward/include????
            //mActionRouter.route(...);
        }        
        
        //
        LogUtil.getLogger().exiting("BaseProcessor","process11");
    }

    protected boolean processForm(IRequest request, IResponse response)
    {
        LogUtil.getLogger().entering("BaseProcessor", "processForm", new Object[]{request, response});

        LogUtil.getLogger().fine("processForm....");

        String pathInfo = request.getPathInfo();
        LogUtil.getLogger().fine("pathInfo=" + pathInfo);

        IParameterParser pp = request.getParameterParser();
        LogUtil.getLogger().fine("parameterParser = " + pp);

        //PageConstants.BCPARAM_FORM_PREFIX 
        //PageConstants.BCPARAM_FIELD_PREFIX 

        TestRForm testRForm1 = (TestRForm) request.getAttribute("testRForm");
        LogUtil.getLogger().fine("TestRForm (request) = " + testRForm1);
        //TestRForm testRForm2 = (TestRForm) request.getSession().getAttribute("testRForm");
        //LogUtil.getLogger().fine("TestRForm (session) = " + testRForm2);
        //TestSForm testSForm1 = (TestSForm) request.getAttribute("testSForm");
        //LogUtil.getLogger().fine("TestSForm (request) = " + testSForm1);
        TestSForm testSForm2 = (TestSForm) request.getSession().getAttribute("testSForm");
        LogUtil.getLogger().fine("TestSForm (session) = " + testSForm2);

        String formname1 = pp.getStringValue("formName");
        LogUtil.getLogger().fine("formname1 = " + formname1);

        // Check if the form has been submitted....
        if(formname1 != null && formname1.equals("testRForm")) {
            IForm  form1 = new TestRForm();
            LogUtil.getLogger().fine("form1 = " + form1);

            PageStatusList ps_pop = form1.populate(request);
            LogUtil.getLogger().fine("ps_pop = " + ps_pop);
            PageStatusList ps_val = form1.validate(request);
            LogUtil.getLogger().fine("ps_val = " + ps_val);

            if(ps_val.isError()) {
                request.setAttribute("testRForm", form1);
                LogUtil.getLogger().fine("TestRForm (request) = " + form1);

                try {
                    IHeaderParser hp = request.getHeaderParser();
                    LogUtil.getLogger().fine("headerParser = " + hp);
                    String refURI = hp.getStringValue("referer");
                    response.sendRedirect(response.encodeRedirectURL(refURI));
                    return true;
                } catch(IOException iox) {
                    iox.printStackTrace();
                }
            }
        }

        String formname2 = pp.getStringValue("formName");
        LogUtil.getLogger().fine("formname2 = " + formname2);
        IForm  form2 = (IForm) request.getSession().getAttribute(formname2);
        LogUtil.getLogger().fine("form2 = " + form2);
        if(form2 != null) {
            PageStatusList ps_pop = form2.populate(request);
            LogUtil.getLogger().fine("ps_pop = " + ps_pop);
            PageStatusList ps_val = form2.validate(request);
            LogUtil.getLogger().fine("ps_val = " + ps_val);

            if(ps_val.isError()) {
                try {
                    IHeaderParser hp = request.getHeaderParser();
                    LogUtil.getLogger().fine("headerParser = " + hp);
                    String refURI = hp.getStringValue("referer");
                    response.sendRedirect(response.encodeRedirectURL(refURI));
                    return true;
                } catch(IOException iox) {
                    iox.printStackTrace();
                }
            }
        }


        //TestRForm testRForm3 = new TestRForm();
        //request.setAttribute("testRForm", testRForm3);
        //LogUtil.getLogger().fine("TestRForm3 (request) = " + testRForm3);

        //TestSForm testSForm4 = new TestSForm();
        //request.getSession().setAttribute("testSForm", testSForm4);
        //LogUtil.getLogger().fine("TestSForm4 (session) = " + testSForm4);

        
        
        
        LogUtil.getLogger().exiting("BaseProcessor", "processForm");
        return false;
    }

    protected void processParameters(IRequest request, IResponse response)
    {
        LogUtil.getLogger().entering("BaseProcessor", "processParameters", new Object[]{request, response});

        ParameterParser paramParser = new ParameterParser(request);
        LogUtil.getLogger().fine("ParameterParser = " + paramParser);
        ((BaseRequest) request).setParameterParser(paramParser);
        //IParameterParser pp = request.getParameterParser();
        //LogUtil.getLogger().fine("parameterParser = " + pp);

        LogUtil.getLogger().exiting("BaseProcessor", "processParameters");
    }

    protected void processCookies(IRequest request, IResponse response)
    {
        LogUtil.getLogger().entering("BaseProcessor", "processCookies", new Object[]{request, response});

        CookieParser cParser = new CookieParser(request);
        LogUtil.getLogger().fine("CookieParser = " + cParser);
        ((BaseRequest) request).setCookieParser(cParser);
        //CookieParser cp = request.getCookieParser();
        //LogUtil.getLogger().fine("cookieParser = " + cp);

        LogUtil.getLogger().exiting("BaseProcessor", "processCookies");
    }

    protected void processHeaders(IRequest request, IResponse response)
    {
        LogUtil.getLogger().entering("BaseProcessor", "processHeaders", new Object[]{request, response});

        HeaderParser hParser = new HeaderParser(request);
        LogUtil.getLogger().fine("HeaderParser = " + hParser);
        ((BaseRequest) request).setHeaderParser(hParser);
        //HeaderParser hp = request.getHeaderParser();
        //LogUtil.getLogger().fine("headerParser = " + hp);


        // locale
        processLocaleHeader(request, response);

        // cache
        processCacheHeader(request, response);

        LogUtil.getLogger().exiting("BaseProcessor", "processHeaders");
    }

    protected void processLocaleHeader(IRequest request, IResponse response)
    {
        // locale
        // content
        //
        
    }


    protected void processCacheHeader(IRequest request, IResponse response)
    {
        // reqeust...

        //if (appConfig.isNoCache()) {
            setCacheHeaders(request,response, 0);
        //} else {
            //
        //}
    }



    public static void setCacheHeaders(IRequest request,
                                       IResponse response,
                                       int expiry)
    {
        if ( expiry == 0 )
        {
            response.setHeader("Pragma", "No-cache");
            response.setHeader("Cache-Control", "no-cache");
            response.setDateHeader("Expires", 1);
            //response.setHeader("Expires", HttpUtils.formatHttpDate(new Date()));
        }
        else
        {
            Date expiryDate = new Date( System.currentTimeMillis() + expiry );
            //response.setHeader("Expires", HttpUtils.formatHttpDate(expiryDate));
        }
    }
    

}




