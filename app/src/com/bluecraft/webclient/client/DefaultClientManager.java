/*************************************************************************
 *  Copyright (c) 2002-2003, BlueCraft Software. 
 *  All Rights Reserved. No part of this program may be copied
 *  or used without the express written consent of BlueCraft Software.
 *************************************************************************/
// $Id: DefaultClientManager.java,v 1.2 2003/02/02 23:12:10 hyoon Exp $ 

package com.bluecraft.webclient.client;

import com.bluecraft.webclient.*;

import java.util.prefs.Preferences;
import java.util.logging.Logger;


/**   
 *   
 * @author   <A href="mailto:hyoon@bluecraft.com">Hyoungsoo Yoon</A>
 * @version  $Revision: 1.2 $
 */   
public class DefaultClientManager
    implements IClientManager
{
    private static IClientManager s_clientManager = null;
    private IClient  mClient = null;

    private boolean mInitialized = false;

    
    protected DefaultClientManager()
    {
        //
    }

    public static IClientManager getInstance()
    {
        if(s_clientManager == null) {
            s_clientManager = new DefaultClientManager();
        }
        return s_clientManager;
    }

    public void initialize(String clientFilePath)
    {
        if(clientFilePath != null) {
            //
        } else {
            //
        }

        
        //mClient = new Client();
        
        mInitialized = true;
    }

    public IClient getClient(ISession sess)
    {
        //
        return mClient;
    }
    
}



