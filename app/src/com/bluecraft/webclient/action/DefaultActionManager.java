/*************************************************************************
 *  Copyright (c) 2002-2003, BlueCraft Software. 
 *  All Rights Reserved. No part of this program may be copied
 *  or used without the express written consent of BlueCraft Software.
 *************************************************************************/
// $Id: DefaultActionManager.java,v 1.2 2003/02/02 23:12:10 hyoon Exp $ 

package com.bluecraft.webclient.action;

import com.bluecraft.webclient.*;
import com.bluecraft.webclient.mapping.*;


/**   
 *   
 * @author   <A href="mailto:hyoon@bluecraft.com">Hyoungsoo Yoon</A>
 * @version  $Revision: 1.2 $
 */   
public class DefaultActionManager
    implements IActionManager
{
    private static IActionManager s_actionManager = null;

    private IActionMappingList mActionMappingList = null;

    
    protected DefaultActionManager()
    {
        //
        // initialize actionMappingList per Servlet???
        mActionMappingList = new DefaultActionMappingList();
        // temporary
        mActionMappingList.addActionMapping(new ActionMapping(URLPattern.DEFAULT_PATTERN, new SimpleAction()));
        mActionMappingList.addActionMapping(new ActionMapping(new URLPattern("/abc/*"), new SimpleAction()));
        mActionMappingList.addActionMapping(new ActionMapping(new URLPattern("/abc/DEF/*"), new SimpleAction()));
        mActionMappingList.addActionMapping(new ActionMapping(new URLPattern("*.xyz"), new SimpleAction()));
        mActionMappingList.addActionMapping(new ActionMapping(new URLPattern("/templates/*"), new JspAction()));
        //
        //
        
    }

    public static IActionManager getInstance()
    {
        if(s_actionManager == null) {
            s_actionManager = new DefaultActionManager();
        }
        return s_actionManager;
    }
    
    public boolean addActionMapping(URLPattern pattern, IAction action)
    {
        return mActionMappingList.addActionMapping(pattern, action);
    }
    
    public boolean removeActionMapping(URLPattern pattern)
    {
        return false;
        //return mActionMappingList.removeActionMapping(pattern);
    }
    
    public IAction getAction(URLPattern pattern)
    {
        return mActionMappingList.getAction(pattern);
    }
    
    public IAction getAction(String  urlString)
    {
        return mActionMappingList.getAction(urlString);
    }
    
    public IAction getDefaultAction()
    {
        return mActionMappingList.getAction(URLPattern.DEFAULT_PATTERN);
    }

}



