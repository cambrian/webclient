/*************************************************************************
 *  Copyright (c) 2002-2003, BlueCraft Software. 
 *  All Rights Reserved. No part of this program may be copied
 *  or used without the express written consent of BlueCraft Software.
 *************************************************************************/
// $Id: BaseActionRouter.java,v 1.2 2003/02/02 23:12:10 hyoon Exp $ 

package com.bluecraft.webclient.action;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.ResourceBundle;


/**   
 *   
 * @author   <A href="mailto:hyoon@bluecraft.com">Hyoungsoo Yoon</A>
 * @version  $Revision: 1.2 $
 */   
public abstract class BaseActionRouter
    implements IActionRouter
{
    // ??? Store URL rather than its key ???
    private final String  mKey;
    private final boolean mIsForward;

    public BaseActionRouter(String key)
    {
        this(key, true); // forward by default
    }
    
    protected BaseActionRouter(String key, boolean isForward)
    {
        mKey = key;
        mIsForward = isForward;
    }


    public synchronized void route(HttpServletRequest req,
                                   HttpServletResponse res) 
        throws ServletException, java.io.IOException
    {
        //
        // TODO: Use PageMapping???
        //
        
        //ResourceBundle bundle = (ResourceBundle)servlet.getServletConfig().getServletContext().getAttribute("action-mappings");
        //String url = (String)bundle.getObject(mKey);

        String url = "templates/about.jsp";

        
        if(mIsForward) {
            req.getRequestDispatcher(res.encodeURL(url)).forward(req, res);
        }
        else {
            res.sendRedirect(res.encodeRedirectURL(url));
        }
    }

    public boolean isForward()
    {
        return mIsForward;
    }

}




