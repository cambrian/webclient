/*************************************************************************
 *  Copyright (c) 2002-2003, BlueCraft Software. 
 *  All Rights Reserved. No part of this program may be copied
 *  or used without the express written consent of BlueCraft Software.
 *************************************************************************/
// $Id: JspActionRouter.java,v 1.2 2003/02/02 23:12:10 hyoon Exp $ 

package com.bluecraft.webclient.action;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.ResourceBundle;


/**   
 *   
 * @author   <A href="mailto:hyoon@bluecraft.com">Hyoungsoo Yoon</A>
 * @version  $Revision: 1.2 $
 */   
public class JspActionRouter
    extends ForwardActionRouter
{

    public JspActionRouter(String key)
    {
        super(key);
    }


    public synchronized void route(HttpServletRequest req,
                                   HttpServletResponse res) 
        throws ServletException, java.io.IOException
    {
        //String url = (String)bundle.getObject(mKey);
        String pathInfo = req.getPathInfo();
        LogUtil.getLogger().fine("pathInfo=" + pathInfo);

        // temporary
        String url = pathInfo;  // getTrailingPath()???

        req.getRequestDispatcher(res.encodeURL(url)).forward(req, res);
    }

}


