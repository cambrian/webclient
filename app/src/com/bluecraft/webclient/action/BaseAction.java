/*************************************************************************
 *  Copyright (c) 2002-2003, BlueCraft Software. 
 *  All Rights Reserved. No part of this program may be copied
 *  or used without the express written consent of BlueCraft Software.
 *************************************************************************/
// $Id: BaseAction.java,v 1.2 2003/02/02 23:12:10 hyoon Exp $ 

package com.bluecraft.webclient.action;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**   
 *   
 * @author   <A href="mailto:hyoon@bluecraft.com">Hyoungsoo Yoon</A>
 * @version  $Revision: 1.2 $
 */   
public abstract class BaseAction
    implements IAction
{
    // Key to the URL...
    private String mKey = null;
    private String mStrURL = null; // ???
    
    public BaseAction()
    {
        // ...
    }
    
    public BaseAction(String key)
    {
        mKey = key;
        //ResourceBundle bundle = (ResourceBundle)servlet.getServletConfig().getServletContext().getAttribute("action-mappings");
        //mStrURL = (String)bundle.getObject(mKey);
    }
    
    protected String getKey()
    {
        return mKey;
    }
  
    // ???
    protected String getURLString()
    {
        return mStrURL;
    }
     
    
//    public IActionRouter perform(HttpServletRequest req, 
//                                 HttpServletResponse res)
//        throws ServletException
//    {
//        LogUtil.getLogger().fine("BaseAction.perform()");
//        return null;
//    }
  
    
}



