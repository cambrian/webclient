/*************************************************************************
 *  Copyright (c) 2002-2003, BlueCraft Software. 
 *  All Rights Reserved. No part of this program may be copied
 *  or used without the express written consent of BlueCraft Software.
 *************************************************************************/
// $Id: DefaultSessionManager.java,v 1.2 2003/02/02 23:12:11 hyoon Exp $ 

package com.bluecraft.webclient.session;

import com.bluecraft.webclient.*;

import javax.servlet.http.HttpSession;
import java.util.Map;
import java.util.HashMap;


/**   
 *   
 * @author   <A href="mailto:hyoon@bluecraft.com">Hyoungsoo Yoon</A>
 * @version  $Revision: 1.2 $
 */   
public class DefaultSessionManager
    implements ISessionManager
{
    private static ISessionManager s_sessionManager = null;
    private Map mSessionMap = null;
    
    protected DefaultSessionManager()
    {
        mSessionMap = new HashMap();
    }

    public static ISessionManager getInstance()
    {
        if(s_sessionManager == null) {
            s_sessionManager = new DefaultSessionManager();
        }
        return s_sessionManager;
    }

    
    public boolean addSession(ISession session)
    {
        synchronized(mSessionMap) {
            mSessionMap.put(session.getId(), session);
        }
        // temporary
        return true;
    }

    public boolean removeSession(ISession session)
    {
        if(session == null) {
            return false;
        }
        Object obj = null;
        synchronized(mSessionMap) {
            String id = session.getId();
            session.expire();
            obj = mSessionMap.remove(id);
        }
        if(obj == null) {
            return false;
        } else {
            return true;
        }
    }

    public boolean removeSession(String sessionID)
    {
        if(sessionID == null) {
            return false;
        }
        synchronized(mSessionMap) {
            ISession sess = (ISession) mSessionMap.remove(sessionID);
            if(sess == null) {
                return false;
            } else {
                sess.expire();
                return true;
            }
        }
    }

    public boolean expireSession(ISession session)
    {
        ISession sess = lookupSession(session);
        if(sess == null) {
            return false;
        } else {
            sess.expire();
            return true;
        }
    }

    public boolean expireSession(String sessionID)
    {
        ISession sess = lookupSession(sessionID);
        if(sess == null) {
            return false;
        } else {
            sess.expire();
            return true;
        }
    }

    public ISession lookupSession(ISession session)
    {
        //synchronized(mSessionMap) {
            if(mSessionMap.containsValue(session)) {
                return session;
            } else {
                return null;
            }
        //}
    }

    public ISession lookupSession(String sessionID)
    {
        //synchronized(mSessionMap) {
            if(!mSessionMap.containsKey(sessionID)) {
                return null;
            }
            ISession sess = (ISession) mSessionMap.get(sessionID);
            return sess;
        //}
    }

    public ISession lookupSession(IRequest request)
    {
        HttpSession hSess = request.getSession(false);
        if(hSess == null) {
            // create
            hSess = request.getSession();
            // add
        }

        ISession iSess = new BaseSession(hSess);
        addSession(iSess);

        return iSess;
    }

    public ISession lookupSession(IRequest request, boolean create)
    {
        HttpSession hSess = request.getSession(false);
        if(create) {
            if(hSess == null) {
                // create
                hSess = request.getSession();
                // add
            }
        } else {
            //
        }
        ISession iSess = null;
        if(hSess != null) {
            iSess = new BaseSession(hSess);
            addSession(iSess);
        }

        return iSess;
    }


}



