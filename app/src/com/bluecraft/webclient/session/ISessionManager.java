/*************************************************************************
 *  Copyright (c) 2002-2003, BlueCraft Software. 
 *  All Rights Reserved. No part of this program may be copied
 *  or used without the express written consent of BlueCraft Software.
 *************************************************************************/
// $Id: ISessionManager.java,v 1.2 2003/02/02 23:12:11 hyoon Exp $ 

package com.bluecraft.webclient.session;

import com.bluecraft.webclient.*;


/**   
 *   
 * @author   <A href="mailto:hyoon@bluecraft.com">Hyoungsoo Yoon</A>
 * @version  $Revision: 1.2 $
 */   
public interface ISessionManager
{
    boolean addSession(ISession session);
    boolean removeSession(ISession session);
    boolean removeSession(String sessionID);
    boolean expireSession(ISession session);
    boolean expireSession(String sessionID);
    ISession lookupSession(ISession session);
    ISession lookupSession(String sessionID);
    ISession lookupSession(IRequest request);
    ISession lookupSession(IRequest request, boolean create);

}



