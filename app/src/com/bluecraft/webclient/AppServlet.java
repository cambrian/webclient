/*************************************************************************
 *  Copyright (c) 2002-2003, BlueCraft Software. 
 *  All Rights Reserved. No part of this program may be copied
 *  or used without the express written consent of BlueCraft Software.
 *************************************************************************/
// $Id: AppServlet.java,v 1.2 2003/02/02 23:12:10 hyoon Exp $ 

package com.bluecraft.webclient;

import com.bluecraft.webclient.processor.*;
import com.bluecraft.webclient.config.*;

import java.io.InputStream;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.MissingResourceException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.UnavailableException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;


/**   
 *   
 * @author   <A href="mailto:hyoon@bluecraft.com">Hyoungsoo Yoon</A>
 * @version  $Revision: 1.2 $
 */   
public class AppServlet
    extends BaseServlet
    implements AppServletConstants
{

    public void destroy()
    {
        super.destroy();
    }

    public void init()
        throws ServletException
    {
        super.init();
        synchronized(this) {
            //
            initServlet();
            //
        }
    }


    private void initServlet()
        throws ServletException
    {
        // Read configuration
        //String configFile = getServletConfig().getInitParameter("CONFIG_FILE_PATH");
        //LogUtil.getLogger().fine("CONFIG_FILE_PATH = " + configFile);
        
        //((DefaultConfigManager) mConfigManager).initialize(configFile);

    }

    protected IProcessor initializeProcessor()
    {
        return new AppProcessor();
    }


    public String getServletInfo()
    {
        return "...";
    }


//    protected void process(HttpServletRequest request,
//                           HttpServletResponse response)
//        throws IOException, ServletException
//    {
//        super.process(request,response);
//        //
//        LogUtil.getLogger().fine("AppServlet.process()");
//        //
//        
//    }

}




