/*************************************************************************
 *  Copyright (c) 2002-2003, BlueCraft Software. 
 *  All Rights Reserved. No part of this program may be copied
 *  or used without the express written consent of BlueCraft Software.
 *************************************************************************/
// $Id: Anchor.java,v 1.2 2003/02/02 23:12:10 hyoon Exp $ 

package com.bluecraft.webclient;

import javax.servlet.http.HttpServletRequest;
import java.net.URL;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;


/**   
 * Anchor represents the HTML &lt;A&gt; tag.
 *
 * @author   <A href="mailto:hyoon@bluecraft.com">Hyoungsoo Yoon</A>
 * @version  $Revision: 1.2 $
 */   
public class Anchor
{
    public final static String METHOD_GET = "get";
    public final static String METHOD_POST = "post";

    public final static String FRAME_SELF = "_self";
    public final static String FRAME_TOP  = "_top";

    private final static String DEFAULT_HREF = "";
    private final static String ROOT_HREF = "/";
    public static Anchor DEFAULT_ANCHOR = null;
    public static Anchor ROOT_ANCHOR = null;

    static {
        DEFAULT_ANCHOR = new Anchor(DEFAULT_HREF);
        ROOT_ANCHOR = new Anchor(ROOT_HREF);
    }

    private URI    mURI;
    private String mTargetFrame;
    private String mMethod;


    public Anchor()
    {
        this(DEFAULT_HREF);
    }
    
    public Anchor(String hRef)
    {
        this(hRef, FRAME_SELF);
    }
    
    public Anchor(String hRef, String targetFrame)
    {
        this(hRef, targetFrame, METHOD_GET);
    }

    public Anchor(String hRef, String targetFrame, String method)
    {
        setURI(hRef);
        mTargetFrame = targetFrame;
        mMethod = method;
    }

    public URI getURI()
    {
        return mURI;
    }

    public URL getURL()
        throws MalformedURLException
    {
        return mURI.toURL();
    }

    public String getHRef()
    {
        return mURI.toString();
    }

    public void setURI(URI uri)
    {
        mURI = uri;
    }

    public void setURI(URL url)
    {
        setURI(url.toString());
    }

    public void setURI(String hRef)
    {
        try {
            mURI = new URI(hRef);
        } catch(URISyntaxException usx1) {
            //usx1.printStackTrace();
            try {
                mURI = new URI(ROOT_HREF);
            } catch(URISyntaxException usx2) {
                usx2.printStackTrace();
                //mURI = null; // what to do???
            }
        }
    }


    public void setURI(HttpServletRequest req, String spec)
    {
        try {
            String hRef = req.getContextPath() + req.getServletPath() + spec;
            mURI = new URI(hRef);
        } catch(URISyntaxException usx1) {
            //usx1.printStackTrace();
            try {
                mURI = new URI(ROOT_HREF);
            } catch(URISyntaxException usx2) {
                usx2.printStackTrace();
                //mURI = null; // what to do???
            }
        }
    }


    public String getAbsoluteHRef(HttpServletRequest req)
    {
        if(mURI == null || req == null) {
            // ???
            return "";
        }
        if(mURI.isAbsolute()) {
            return mURI.toString();
        } else {
            URI uri = null;
            try {
                uri = mURI.resolve(new URI(req.getRequestURI()));
            } catch(URISyntaxException usx) {
                //usx.printStackTrace();
                uri = URI.create(ROOT_HREF);
            }
            return uri.toString();
        }
    }

    public String getRelativeHRef(HttpServletRequest req)
    {
        if(mURI == null || req == null) {
            // ???
            return "";
        }
        URI uri = null;
        try {
            uri = mURI.relativize(new URI(req.getRequestURI()));
        } catch(URISyntaxException usx) {
            //usx.printStackTrace();
            uri = URI.create(DEFAULT_HREF);
        }
        return uri.toString();
    }

    public String getTargetFrame()
    {
        return mTargetFrame;
    }

    public void setTargetFrame(String targetFrame)
    {
        mTargetFrame = targetFrame;
    }

    public String getMethod()
    {
        return mMethod;
    }

    public void setMethod(String method)
    {
        mMethod = method;
    }

}



