/*************************************************************************
 *  Copyright (c) 2002-2003, BlueCraft Software. 
 *  All Rights Reserved. No part of this program may be copied
 *  or used without the express written consent of BlueCraft Software.
 *************************************************************************/
// $Id: CookieUtil.java,v 1.2 2003/02/02 23:12:11 hyoon Exp $ 


package com.bluecraft.webclient.util;

import com.bluecraft.webclient.*;

import javax.servlet.Servlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Cookie;


/**   
 *   
 * @author   <A href="mailto:hyoon@bluecraft.com">Hyoungsoo Yoon</A>
 * @version  $Revision: 1.2 $
 */   
public class CookieUtil
{
    public static final int COOKIE_AGE_SESSION = -1;
    public static final int COOKIE_AGE_DELETE = 0;

    
    private CookieUtil()
    {
        //
    }

    

    public static void set(HttpServletResponse res, String name, String value, int seconds_age)
    {
        Cookie cookie = new Cookie (name,value);
        cookie.setMaxAge (seconds_age);
        //cookie.setPath (request.getServletPath());
        res.addCookie (cookie);
    }

    public static void unset(HttpServletResponse res, String name)
    {
        set(res, name," ",COOKIE_AGE_DELETE);
    }
    
    
}



