/*************************************************************************
 *  Copyright (c) 2002-2003, BlueCraft Software. 
 *  All Rights Reserved. No part of this program may be copied
 *  or used without the express written consent of BlueCraft Software.
 *************************************************************************/
// $Id: WebUtil.java,v 1.2 2003/02/02 23:12:11 hyoon Exp $ 

package com.bluecraft.webclient.util;

import com.bluecraft.webclient.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


/**   
 * WebUtil implements some convenience functions
 *    for web-related classes
 *   
 * @author   <A href="mailto:hyoon@bluecraft.com">Hyoungsoo Yoon</A>
 * @version  $Revision: 1.2 $
 */
public class WebUtil
{
    // Prevents the instantiation
    private WebUtil()
    {
    }

    public static IRequest getIRequest(HttpServletRequest req)
    {
        if(req instanceof IRequest) {
            return (IRequest) req;
        } else {
            return new BaseRequest(req);
        }
    }

    public static boolean isIRequest(HttpServletRequest req)
    {
        if(req instanceof IRequest) {
            return true;
        } else {
            return false;
        }
    }

    public static IResponse getIResponse(HttpServletResponse res)
    {
        if(res instanceof IResponse) {
            return (IResponse) res;
        } else {
            return new BaseResponse(res);
        }
    }

    public static boolean isIResponse(HttpServletResponse req)
    {
        if(req instanceof IResponse) {
            return true;
        } else {
            return false;
        }
    }

    public static ISession getISession(HttpSession sess)
    {
        if(sess instanceof ISession) {
            return (ISession) sess;
        } else {
            return new BaseSession(sess);
        }
    }

    public static boolean isISession(HttpSession req)
    {
        if(req instanceof ISession) {
            return true;
        } else {
            return false;
        }
    }

}



