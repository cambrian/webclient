/*************************************************************************
 *  Copyright (c) 2002-2003, BlueCraft Software. 
 *  All Rights Reserved. No part of this program may be copied
 *  or used without the express written consent of BlueCraft Software.
 *************************************************************************/
// $Id: HeartBeat.java,v 1.2 2003/02/02 23:12:11 hyoon Exp $ 

package com.bluecraft.webclient.util;

import com.bluecraft.webclient.*;

import java.util.*;
import java.lang.reflect.*;


/**   
 *
 *   
 * @author   <A href="mailto:hyoon@bluecraft.com">Hyoungsoo Yoon</A>
 * @version  $Revision: 1.2 $
 */
public class HeartBeat
    extends Thread
    implements Beatable
{
    private static final long DEFAULT_INTERVAL = 300000;
    
    private long  mInterval = DEFAULT_INTERVAL;
    private Map   mBeepers = new HashMap();
    private boolean mMarkedToBeStopped = false;
        
    public HeartBeat()
    {
        this(DEFAULT_INTERVAL);
    }

    public HeartBeat(long interval)
    {
        super();
        mInterval = interval;
    }

    public HeartBeat(String s)
    {
        this(s, DEFAULT_INTERVAL);
    }

    public HeartBeat(String s, long interval)
    {
        super(s);
        mInterval = interval;
    }

    public HeartBeat(ThreadGroup tg, String s)
    {
        this(tg, s, DEFAULT_INTERVAL);
    }

    public HeartBeat(ThreadGroup tg, String s, long interval)
    {
        super(tg, s);
        mInterval = interval;
    }


    public long getInterval()
    {
        return mInterval;
    }

    public void setInterval(long interval)
    {
        mInterval = interval;
    }

    
    public void beat()
    {
        LogUtil.getLogger().fine("beat()");
        //
        // do something with mBeepers...
        //

    }
    
    public void addCallback(Beepable beeper)
    {
        mBeepers.put(new Long(beeper.getInterval()), beeper);
    }

    public void markToBeStopped()
    {
        mMarkedToBeStopped = true;
    }
    
    public void run()
    {
        while(!mMarkedToBeStopped) {
            try {
                try {
                    //
                    Thread.sleep(mInterval);
                } finally {
                    //
                }
                beat();
            } catch (InterruptedException e) {
                //...
            }
        }
    }


}



