/*************************************************************************
 *  Copyright (c) 2002-2003, BlueCraft Software. 
 *  All Rights Reserved. No part of this program may be copied
 *  or used without the express written consent of BlueCraft Software.
 *************************************************************************/
// $Id: CommonUtil.java,v 1.2 2003/02/02 23:12:11 hyoon Exp $ 

package com.bluecraft.webclient.util;

import com.bluecraft.webclient.*;

import java.util.*;
import java.lang.reflect.*;


/**   
 * CommonUtil implements some convenience functions
 *    common to all Objects.
 *   
 * @author   <A href="mailto:hyoon@bluecraft.com">Hyoungsoo Yoon</A>
 * @version  $Revision: 1.2 $
 */
public class CommonUtil
{
    // Prevents the instantiation
    private CommonUtil()
    {
    }


    /**
     * This method prints out all "attributes" of the given object.
     * "Attributes" are defined to be those fields of the object
     * that can be accessed through public "getters". 
     */
    public static String toString(Object obj)
    {
        if(obj == null) {
            return null;
        }
        
        Class klass = obj.getClass();

        Method[] methods = null;
        try {
            //fields = klass.getDeclaredMethods();
            methods = klass.getMethods();
        } catch(SecurityException sx) {
            //sx.printStackTrace();
        }
        
        if(methods == null) {
            return obj.toString();
        } else {       
            StringBuffer sb = new StringBuffer();
            
            for(int i=0;i<methods.length;i++) {
                String name = methods[i].getName();
                if(name.indexOf("get") == 0 
                   && methods[i].getParameterTypes().length == 0
                   && !Modifier.isStatic(methods[i].getModifiers())) {
                    String fieldName = name.substring(3);
                   
                    Object value = null;
                    try {
                        value = methods[i].invoke(obj, null);
                    } catch(IllegalArgumentException ilx) {
                        //ilx.printStackTrace();
                    } catch(IllegalAccessException iax) {
                        //iax.printStackTrace();
                    } catch(InvocationTargetException itx) {
                        //itx.printStackTrace();
                    }
                    sb.append(fieldName);
                    sb.append(":");
                    if(value != null) {               
                        sb.append(value.toString());
                    }  
                    sb.append(";");               
                }     
            }
            
            return sb.toString();
        }
    }

}



