/*************************************************************************
 *  Copyright (c) 2002-2003, BlueCraft Software. 
 *  All Rights Reserved. No part of this program may be copied
 *  or used without the express written consent of BlueCraft Software.
 *************************************************************************/
// $Id: HttpSessionWrapper.java,v 1.2 2003/02/02 23:12:10 hyoon Exp $ 

package com.bluecraft.webclient;


import java.io.InputStream;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.MissingResourceException;
import javax.servlet.ServletContext;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.UnavailableException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionContext;
import javax.sql.DataSource;


/**   
 *   
 * @author   <A href="mailto:hyoon@bluecraft.com">Hyoungsoo Yoon</A>
 * @version  $Revision: 1.2 $
 */   
public class HttpSessionWrapper
    implements HttpSession
{
    private HttpSession mSession;
    
    public HttpSessionWrapper(HttpSession session)
    {
        mSession = session;
    }


    public HttpSession getHttpSession()
    {
        return mSession;
    }

    public java.lang.Object getAttribute(java.lang.String name)
    {
        return mSession.getAttribute(name);
    }

    public java.util.Enumeration getAttributeNames()
    {
        return mSession.getAttributeNames();
    }
    
    public long getCreationTime() 
    {
        return mSession.getCreationTime();
    }
    
    public java.lang.String getId() 
    {
        return mSession.getId();
    }
    
    public long getLastAccessedTime() 
    {
        return mSession.getLastAccessedTime();
    }
    
    public int getMaxInactiveInterval() 
    {
        return mSession.getMaxInactiveInterval();
    }
    
    public ServletContext getServletContext() 
    {
        return mSession.getServletContext();
    }
    
    public HttpSessionContext getSessionContext() 
    {
        return null;
    }
    
    public java.lang.Object getValue(java.lang.String name) 
    {
        return null;
    }
    
    public java.lang.String[] getValueNames() 
    {
        return null;
    }
    
    public void invalidate() 
    {
        mSession.invalidate();
    }
    
    public boolean isNew() 
    {
        return mSession.isNew();
    }
    
    public void putValue(java.lang.String name, java.lang.Object value) 
    {
        //null;
    }
    
    public void removeAttribute(java.lang.String name) 
    {
        mSession.removeAttribute(name); 
    }
    
    public void removeValue(java.lang.String name) 
    {
        //null;
    }
    
    public void setAttribute(java.lang.String name, java.lang.Object value) 
    {
        mSession.setAttribute(name, value);
    }
    
    public void setMaxInactiveInterval(int interval) 
    {
        mSession.setMaxInactiveInterval(interval);
    }
    


}



