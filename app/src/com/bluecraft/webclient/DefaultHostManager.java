/*************************************************************************
 *  Copyright (c) 2002-2003, BlueCraft Software. 
 *  All Rights Reserved. No part of this program may be copied
 *  or used without the express written consent of BlueCraft Software.
 *************************************************************************/
// $Id: DefaultHostManager.java,v 1.2 2003/02/02 23:12:10 hyoon Exp $ 

package com.bluecraft.webclient;

import com.bluecraft.webclient.cache.*;
import com.bluecraft.webclient.resource.*;
import com.bluecraft.common.Guid;

import javax.servlet.ServletContext;
import java.util.logging.Logger;
import java.util.Locale;
import java.util.Map;
import java.util.HashMap;
import java.util.Set;
import java.util.Iterator;


/**
 * DefaultHostManager is a singleton class,
 * which manages all web applications registered in a servlet container.
 *
 * @author   <A href="mailto:hyoon@bluecraft.com">Hyoungsoo Yoon</A>
 * @version  $Revision: 1.2 $
 */   
public class DefaultHostManager
    implements IHostManager
{
    private static IHostManager s_hostManager = null;
    private Map    mWebAppMap = null;

    
    protected DefaultHostManager()
    {
        mWebAppMap = new HashMap();
    }

    public static IHostManager getInstance()
    {
        if(s_hostManager == null) {
            s_hostManager = new DefaultHostManager();
        }
        return s_hostManager;
    }

    public Guid                registerApplication(ServletContext servletContext)
    {
        LogUtil.getLogger().entering("DefaultHostManager","registerApplication", new Object[]{servletContext});

        Guid contextID = null;        
        IApplicationManager appManager = new DefaultApplicationManager(servletContext);
        if(mWebAppMap.containsValue(appManager)) {
            Set kset = mWebAppMap.keySet();
            Iterator kit = kset.iterator();
            while(kit.hasNext()) {
                Guid id = (Guid) kit.next();
                //LogUtil.getLogger().fine("guid = " + id);
                IApplicationManager am = (IApplicationManager) mWebAppMap.get(id);
                if(appManager.equals(am)) {
                    contextID = id;
                    break;
                }
            }
        }

        if(contextID == null) {
            contextID = new Guid();
            mWebAppMap.put(contextID, appManager);
        }

        LogUtil.getLogger().exiting("DefaultHostManager","registerApplication", contextID);
        return contextID;
    }

    public IApplicationManager getApplicationManager(Guid contextID)
    {
        return (IApplicationManager) mWebAppMap.get(contextID);
    }

    public IApplicationManager getApplicationManager(ServletContext servletContext)
    {
        Guid contextID = registerApplication(servletContext);
        return getApplicationManager(contextID);
    }

}


