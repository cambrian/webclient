/*************************************************************************
 *  Copyright (c) 2002-2003, BlueCraft Software. 
 *  All Rights Reserved. No part of this program may be copied
 *  or used without the express written consent of BlueCraft Software.
 *************************************************************************/
// $Id: LogUtil.java,v 1.2 2003/02/02 23:12:10 hyoon Exp $ 

package com.bluecraft.webclient.cache;

import com.bluecraft.webclient.*;
import com.bluecraft.webclient.config.*;
import com.bluecraft.webclient.logging.*;
import com.bluecraft.webclient.util.*;

import java.util.logging.Logger;


/**   
 * Utility class for Java logging framework.
 *
 * @author   <A href="mailto:hyoon@bluecraft.com">Hyoungsoo Yoon</A>
 * @version  $Revision: 1.2 $
 */
final class LogUtil
    extends AbstractLogUtil
{
    private static final String sName = "com.bluecraft.webclient.cache";
    
    private LogUtil()
    {
    }

    static Logger getLogger()
    {
        return Logger.getLogger(sName,getResourceBundle());
    }
  
}


