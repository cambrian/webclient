/*************************************************************************
 *  Copyright (c) 2002-2003, BlueCraft Software. 
 *  All Rights Reserved. No part of this program may be copied
 *  or used without the express written consent of BlueCraft Software.
 *************************************************************************/
// $Id: DefaultDBManager.java,v 1.2 2003/02/02 23:12:10 hyoon Exp $ 

package com.bluecraft.webclient.db;

import com.bluecraft.webclient.*;

import java.util.logging.Logger;


/**   
 *   
 * @author   <A href="mailto:hyoon@bluecraft.com">Hyoungsoo Yoon</A>
 * @version  $Revision: 1.2 $
 */   
public class DefaultDBManager
    implements IDBManager
{
    private static IDBManager s_DBManager = null;
    private boolean mInitialized = false;

    
    protected DefaultDBManager()
    {
        //
    }

    public static IDBManager getInstance()
    {
        if(s_DBManager == null) {
            s_DBManager = new DefaultDBManager();
        }
        return s_DBManager;
    }

    protected void initialize(String dbConnectionString)
    {
        if(dbConnectionString != null) {
            //
        } else {
            //
        }

        mInitialized = true;
    }

    
}



