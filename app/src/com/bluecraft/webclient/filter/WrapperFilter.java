/*************************************************************************
 *  Copyright (c) 2002-2003, BlueCraft Software. 
 *  All Rights Reserved. No part of this program may be copied
 *  or used without the express written consent of BlueCraft Software.
 *************************************************************************/
// $Id: WrapperFilter.java,v 1.2 2003/02/02 23:12:10 hyoon Exp $ 

package com.bluecraft.webclient.filter;

import com.bluecraft.webclient.*;
import com.bluecraft.webclient.config.*;
import com.bluecraft.webclient.logging.*;
import com.bluecraft.webclient.util.*;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.Filter;
import javax.servlet.FilterConfig;
import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.logging.Logger;


/**   
 * 
 *
 * @author   <A href="mailto:hyoon@bluecraft.com">Hyoungsoo Yoon</A>
 * @version  $Revision: 1.2 $
 */
public class WrapperFilter
    implements Filter
{
    private FilterConfig mFilterConfig = null;

    public void doFilter(ServletRequest request,
                         ServletResponse response,
                         FilterChain chain)
        throws IOException, ServletException
    {
        LogUtil.getLogger().entering("WrapperFilter", "doFilter", new Object[]{request, response, chain});
        
        if(request == null) {
            throw new IllegalArgumentException("Request cannot be null");
        } else if(!(request instanceof HttpServletRequest)) {
            throw new IllegalArgumentException("Request is not an instance of HttpServletRequest");
        }             
        if(response == null) {
            throw new IllegalArgumentException("Response cannot be null");
        } else if(!(response instanceof HttpServletResponse)) {
            throw new IllegalArgumentException("Response is not an instance of HttpServletResponse");
        }

        IRequest req = new BaseRequest((HttpServletRequest) request);
        IResponse res = new BaseResponse((HttpServletResponse) response);
        //LogUtil.getLogger().info("request = " + req);
        
        // ...
                
        LogUtil.getLogger().info("request = " + req);
        chain.doFilter(req, res);
        LogUtil.getLogger().info("response = " + res);
                
        // ...

        //LogUtil.getLogger().info("response = " + res);
        LogUtil.getLogger().exiting("WrapperFilter", "doFilter");
    }

    public void destroy()
    {
        this.mFilterConfig = null;
    }

    public void init(FilterConfig filterConfig)
    {
        this.mFilterConfig = filterConfig;
    }

  
}



