/*************************************************************************
 *  Copyright (c) 2002-2003, BlueCraft Software. 
 *  All Rights Reserved. No part of this program may be copied
 *  or used without the express written consent of BlueCraft Software.
 *************************************************************************/
// $Id: BaseForm.java,v 1.2 2003/02/02 23:12:10 hyoon Exp $ 

package com.bluecraft.webclient.page;

import com.bluecraft.webclient.*;
import com.bluecraft.webclient.databean.*;
import com.bluecraft.webclient.parser.*;
import com.bluecraft.webclient.mapping.*;

import com.bluecraft.common.Guid;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;


/**   
 *   
 * @author   <A href="mailto:hyoon@bluecraft.com">Hyoungsoo Yoon</A>
 * @version  $Revision: 1.2 $
 */   
public class BaseForm
    extends BasePageFragment
    implements IForm
{
    private static final String SET      = "set";
    private static final String PROCESS  = "process";

    private boolean mIsPopulated = false;
    private boolean mIsValidated = false;
    private Guid mID = null;

    private IFormBean mFormBean = null;

    // TODO:
    private Anchor mActionAnchor = null;
    private String mContextPath = null;
    private String mServletPath = null;
    private String mPathInfo = null;

    // TODO:
    private URL mURLOnSuccess = null;
    private URL mURLOnFailure = null;


    public BaseForm()
    {
        super();
        mID = new Guid();
        //mActionAnchor = new Anchor();
        
        // TODO:
        // Register myself to form_store....
        // ???
    }


    public boolean    isPopulated()
    {
        return mIsPopulated;
    }

    public boolean    isValidated()
    {
        return mIsValidated;
    }

    public Anchor getActionAnchor()
    {
        if(mActionAnchor == null) {
            mActionAnchor = Anchor.DEFAULT_ANCHOR;
        }
        return mActionAnchor;
    }

    public URI getActionURI()
    {
        return getActionAnchor().getURI();
    }

    public URL getActionURL()
        throws MalformedURLException
    {
        return getActionAnchor().getURL();
    }

    public String getActionHRef()
    {
        return getActionAnchor().getHRef();
    }

    public void setActionAnchor(Anchor anchor)
    {
        mActionAnchor = anchor;
    }

    public void setActionURI(URL url)
    {
        getActionAnchor().setURI(url);
    }

    public void setActionURI(String strURI)
    {
        getActionAnchor().setURI(strURI);
    }

    public void setActionURI(HttpServletRequest req, String spec)
    {
        getActionAnchor().setURI(req, spec);
    }


    public Guid getGuid()
    {
        return mID;
    }

    public String getID()
    {
        if(mID == null) {
            return Guid.NULL_GUID.toString();
        }
        return mID.toString();
    }


    public String getURLOnSuccess()
    {
        if(mURLOnSuccess == null) {
            return "";
        }
        return mURLOnSuccess.toString();
    }

    protected void setURLOnSuccess(String strURL)
    {
        try {
            mURLOnSuccess = new URL(strURL);
        } catch(MalformedURLException mux) {
            mux.printStackTrace();
        }
    }

    public String getURLOnFailure()
    {
        if(mURLOnFailure == null) {
            return "";
        }
        return mURLOnFailure.toString();
    }

    
    protected void setURLOnFailure(String strURL)
    {
        try {
            mURLOnFailure = new URL(strURL);
        } catch(MalformedURLException mux) {
            mux.printStackTrace();
        }
    }


    public void reset()
    {
        mIsPopulated = false;
        mIsValidated = false;
    }

    
    public IFormBean getFormBean()
    {
        return mFormBean;
    }

    public void setFormBean(IFormBean formBean)
    {
        mFormBean = formBean;
    }

    
    protected void setField(String name, IParameterParser parser)
        //throws IllegalStateException
    {
        if(name == null || name.length() < 1) {
            throw new IllegalStateException("Field name is null!");
        }
        String before = name.substring(0,1);
        String after = name.substring(1);
        String canonicalName = before.toUpperCase() + after;

        String methodName = SET + canonicalName;
        //LogUtil.getLogger().fine("methodName = " + methodName);
        //Class[] parameterTypes = {parser.getClass()};

        try {
            //Class[] parameterTypes = {Class.forName("com.bluecraft.webclient.parser.IParameterParser"),
            //						  Class.forName("com.bluecraft.webclient.page.PageStatus")
            //						 };
            Class[] parameterTypes = {Class.forName("java.lang.String")};

            Method set = this.getClass().getMethod(methodName, parameterTypes);
            Object[] parameters = {parser.getStringValue(name)};
            set.invoke(this, parameters);
        } catch (NoSuchMethodException ne) {
            //throw new IllegalStateException ("No set method exists for property " + name + " in implementation ");
            // ignore
            //LogUtil.getLogger().fine("No set method exists for property " + name + " in implementation ");
        } catch (Exception e) {
        	//e.printStackTrace();
            throw new IllegalStateException(e.getMessage());
        }
    }


    public PageStatusList populate(IRequest req)
        //throws IllegalStateException
    {
        LogUtil.getLogger().entering("BaseForm", "populate", new Object[]{req});

        PageStatusList status = new PageStatusList();
        IParameterParser parser = req.getParameterParser();

        String[] names = null;
        names = parser.keys();

        LogUtil.getLogger().fine("names = " + names);

        if (names != null) {
            for (int i = 0; i < names.length; i++) {
                setField(names[i], parser);
            }
        }

        // 
        mIsPopulated = true;

        LogUtil.getLogger().exiting("BaseForm", "populate", status);
        return status;
    }


    // temporary
    private static int counter = 0;
    
    public PageStatusList validate(IRequest req)
    {
        LogUtil.getLogger().entering("BaseForm", "validate", new Object[]{req});

        PageStatusList status = new PageStatusList();
        IParameterParser parser = req.getParameterParser();

        
        /// ?????
        if((++counter)%2 == 0) {
            status = new PageStatusList(new PageStatus(PageStatusCodes.PSC_SUCCESS));
        } else {
            status = new PageStatusList(new PageStatus(PageStatusCodes.PSC_INVALID_PARAMETER)); 
        }
        /// ?????
        

        //
        mIsValidated = true;

        LogUtil.getLogger().exiting("BaseForm", "validate", status);
        return status;
    }


    protected boolean  getCheckBoxValue(String fieldName)
    {
        return false;
    }
    
    protected boolean  getRadioButtonValue(String groupName, String fieldName)
    {
        return false;
    }
    
    protected String   getTextFieldValue(String fieldName)
    {
        return "";
    }

    protected String   getTextAreaValue(String fieldName)
    {
        return "";
    }

    protected String   getPasswordFieldValue(String fieldName)
    {
        return "";
    }

    protected String   getComboBoxValue(String fieldName)
    {
        return "";
    }

    protected String[] getListBoxValues(String fieldName)
    {
        return new String[]{};
    }

    
    
}



