/*************************************************************************
 *  Copyright (c) 2002-2003, BlueCraft Software. 
 *  All Rights Reserved. No part of this program may be copied
 *  or used without the express written consent of BlueCraft Software.
 *************************************************************************/
// $Id: IForm.java,v 1.2 2003/02/02 23:12:10 hyoon Exp $ 

package com.bluecraft.webclient.page;

import com.bluecraft.webclient.*;
import com.bluecraft.webclient.databean.*;

import java.net.URI;


/**   
 *   
 * @author   <A href="mailto:hyoon@bluecraft.com">Hyoungsoo Yoon</A>
 * @version  $Revision: 1.2 $
 */   
public interface IForm
    extends IPageFragment, FormConstants
{
    String getID();
    //String getName();

    Anchor getActionAnchor();
    URI    getActionURI();
    String getActionHRef();

    String getURLOnSuccess();
    String getURLOnFailure();

    PageStatusList populate(IRequest req);
    boolean        isPopulated();

    PageStatusList validate(IRequest req);
    boolean        isValidated();

    IFormBean getFormBean();
    //void initialize();
    void reset();



}


