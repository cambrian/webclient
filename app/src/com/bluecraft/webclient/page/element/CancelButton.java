/*************************************************************************
 *  Copyright (c) 2002-2003, BlueCraft Software. 
 *  All Rights Reserved. No part of this program may be copied
 *  or used without the express written consent of BlueCraft Software.
 *************************************************************************/
// $Id: CancelButton.java,v 1.2 2003/02/02 23:12:10 hyoon Exp $

package com.bluecraft.webclient.page.element;

import com.bluecraft.webclient.*;
import com.bluecraft.webclient.page.*;
import com.bluecraft.webclient.logging.*;
import com.bluecraft.webclient.util.*;


/**   
 *   
 * @author   <A href="mailto:hyoon@bluecraft.com">Hyoungsoo Yoon</A>
 * @version  $Revision: 1.2 $
 */   
public class CancelButton
    extends BaseButton
{
    public CancelButton()
    {
    }

}


