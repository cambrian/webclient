/*************************************************************************
 *  Copyright (c) 2002-2003, BlueCraft Software. 
 *  All Rights Reserved. No part of this program may be copied
 *  or used without the express written consent of BlueCraft Software.
 *************************************************************************/
// $Id: PageStatusCodes.java,v 1.2 2003/02/02 23:12:10 hyoon Exp $ 

package com.bluecraft.webclient.page;


/**   
 * Collection of Page Status Codes.
 * They follow the following convention.
 * <PRE>
    // Success == 0
    // Warning < 0
    // Error > 0
 * </PRE>
 *
 * @author   <A href="mailto:hyoon@bluecraft.com">Hyoungsoo Yoon</A>
 * @version  $Revision: 1.2 $
 */   
public interface PageStatusCodes
{
    ////////////////////
    // Success == 0
    // Warning < 0
    // Error > 0
    ////////////////////

    final static int PSC_SUCCESS = 0;

    final static int PSC_EMPTY_FIELD = -1;

    final static int PSC_REQUIRED_FIELD_MISSING = 1;
    final static int PSC_INVALID_PARAMETER = 2;

}


