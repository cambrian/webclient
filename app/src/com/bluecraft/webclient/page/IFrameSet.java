/*************************************************************************
 *  Copyright (c) 2002-2003, BlueCraft Software. 
 *  All Rights Reserved. No part of this program may be copied
 *  or used without the express written consent of BlueCraft Software.
 *************************************************************************/
// $Id: IFrameSet.java,v 1.2 2003/02/02 23:12:10 hyoon Exp $

package com.bluecraft.webclient.page;

import com.bluecraft.webclient.*;


/**   
 * IFrameSet is an abstract representation of HTML FrameSet.
 *
 * @author   <A href="mailto:hyoon@bluecraft.com">Hyoungsoo Yoon</A>
 * @version  $Revision: 1.2 $
 */   
public interface IFrameSet
    extends  IPage, PageConstants
{
    IFrame[] getFrames();
}


