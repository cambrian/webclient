/*************************************************************************
 *  Copyright (c) 2002-2003, BlueCraft Software. 
 *  All Rights Reserved. No part of this program may be copied
 *  or used without the express written consent of BlueCraft Software.
 *************************************************************************/
// $Id: PageStatus.java,v 1.2 2003/02/02 23:12:10 hyoon Exp $ 

package com.bluecraft.webclient.page;

import com.bluecraft.webclient.*;


/**   
 *   
 * @author   <A href="mailto:hyoon@bluecraft.com">Hyoungsoo Yoon</A>
 * @version  $Revision: 1.2 $
 */   
public class PageStatus
    implements PageStatusCodes
{
    int     mCode;
    String  mMessage;
    
    public PageStatus()
    {
        this(PSC_SUCCESS);
    }
    
    public PageStatus(int code)
    {
        this(code,null);
    }
    
    public PageStatus(int code, String msg)
    {
        mCode = code;
        mMessage = msg;
    }
    
    public int      getCode()
    {
        return mCode;
    }

    public String   getMessage()
    {
        return mMessage;
    }

    public boolean  isSuccess()
    {
        if(mCode == PSC_SUCCESS) {
            return true;
        } else {
            return false;
        }
    }

    public boolean  isError()
    {
        if(mCode > 0) {
            return true;
        } else {
            return false;
        }
    }

    public boolean  isWarning()
    {
        if(mCode < 0) {
            return true;
        } else {
            return false;
        }
    }

    public String toString()
    {
        // TODO:
        return "Code=" + mCode + ";Message=" + mMessage;
    }

    public int hashCode()
    {
        return mCode + mMessage.hashCode();
    }

}



