/*************************************************************************
 *  Copyright (c) 2002-2003, BlueCraft Software. 
 *  All Rights Reserved. No part of this program may be copied
 *  or used without the express written consent of BlueCraft Software.
 *************************************************************************/
// $Id: TestRForm.java,v 1.2 2003/02/02 23:12:10 hyoon Exp $ 

package com.bluecraft.webclient.page;

import com.bluecraft.webclient.*;
import com.bluecraft.webclient.parser.*;
import com.bluecraft.webclient.mapping.*;


/**   
 *   
 * @author   <A href="mailto:hyoon@bluecraft.com">Hyoungsoo Yoon</A>
 * @version  $Revision: 1.2 $
 */   
public class TestRForm
    extends BaseRForm
{
    private String testFieldValue = null;
    
    public TestRForm()
    {
        super();
        testFieldValue = "aaa";

        // temporary
        String strURL = "/appservlet/templates/blank.jsp";
        Anchor anchor = null;
        try {
            anchor = new Anchor(strURL);
        } catch(Exception ex) {
            ex.printStackTrace();
            anchor = new Anchor(); // ???
        }
        setActionAnchor(anchor);
    }


    public void setFormName(String name)
    {
        LogUtil.getLogger().fine("setFormName() name = " + name);

        // TODO:
        // Store as req attribute....
        //
        
    }
    
    public String   getTestFieldValue()
    {
        //LogUtil.getLogger().fine("getTestFieldValue()");

        
        return testFieldValue;
    }

    public void   setTestFieldValue(String value)
    {
        LogUtil.getLogger().fine("setTestFieldValue()");

        testFieldValue = value;

    }
    
    
}



