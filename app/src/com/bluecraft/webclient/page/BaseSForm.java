/*************************************************************************
 *  Copyright (c) 2002-2003, BlueCraft Software. 
 *  All Rights Reserved. No part of this program may be copied
 *  or used without the express written consent of BlueCraft Software.
 *************************************************************************/
// $Id: BaseSForm.java,v 1.2 2003/02/02 23:12:10 hyoon Exp $ 

package com.bluecraft.webclient.page;

import com.bluecraft.webclient.*;
import com.bluecraft.webclient.parser.*;
import com.bluecraft.webclient.mapping.*;

import com.bluecraft.common.Guid;

import javax.servlet.http.HttpSession;
import java.lang.reflect.Method;


/**   
 * Form "Bean" interface to be used in <B>S</B>ession scope.
 *   
 * @author   <A href="mailto:hyoon@bluecraft.com">Hyoungsoo Yoon</A>
 * @version  $Revision: 1.2 $
 */   
public class BaseSForm
    extends BaseForm
    implements ISForm
{
    private boolean mIsModified = false;
    private HttpSession mSession = null;  // cache it???

    public BaseSForm()
    {
        this(false);
    }

    public BaseSForm(boolean isModified)
    {
        super();
        mIsModified = isModified;
    }

    public boolean isModified()
    {
        return mIsModified;
    }

}



