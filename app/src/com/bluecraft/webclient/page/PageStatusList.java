/*************************************************************************
 *  Copyright (c) 2002-2003, BlueCraft Software. 
 *  All Rights Reserved. No part of this program may be copied
 *  or used without the express written consent of BlueCraft Software.
 *************************************************************************/
// $Id: PageStatusList.java,v 1.2 2003/02/02 23:12:10 hyoon Exp $ 

package com.bluecraft.webclient.page;

import com.bluecraft.webclient.*;

import java.util.List;
import java.util.ArrayList;


/**   
 *   
 * @author   <A href="mailto:hyoon@bluecraft.com">Hyoungsoo Yoon</A>
 * @version  $Revision: 1.2 $
 */   
public class PageStatusList
    implements PageStatusCodes
{
    private List mList = null;
    
    public PageStatusList()
    {
        this((PageStatus) null);
    }

    public PageStatusList(PageStatus pageStatus)
    {
        mList = new ArrayList();
        addPageStatus(pageStatus);
    }

    public PageStatusList(PageStatusList pageStatusList)
    {
        mList = new ArrayList();
        addPageStatusList(pageStatusList);
    }

    public List getList()
    {
        return mList;
    }

    public int size()
    {
        return mList.size();
    }

    public boolean containsPageStatus(PageStatus pageStatus)
    {
        if(pageStatus != null) {
            return mList.contains(pageStatus);
        }
        return false;
    }

    public PageStatus getPageStatus(int idx)
    {
        return (PageStatus) mList.get(idx);
    }
    
    public boolean addPageStatus(PageStatus pageStatus)
    {
        if(pageStatus != null) {
            return mList.add(pageStatus);
        }
        return false;
    }

    public boolean addPageStatusList(PageStatusList pageStatusList)
    {
        if(pageStatusList != null) {
            //return mList.addAll(pageStatusList.getList());
            int len = pageStatusList.size();
            if(len == 0) {
                return false;
            }
            boolean bSuc = true;
            for(int i=0;i<len;i++) {
                if(!addPageStatus(getPageStatus(i))) {
                    bSuc = false;
                }
            }
            return bSuc;
        }
        return false;
    }

    public boolean  isSuccess()
    {
        //return (!containsError() && !containsWarning());
        for(int i=0;i<size();i++) {
            if(!getPageStatus(i).isSuccess()) {
                return false;
            }
        }
        return true;
    }

    public boolean  isError()
    {
        return containsError();
    }

    public boolean  isWarning()
    {
        return (!containsError() && containsWarning());
    }

    public boolean  containsError()
    {
        for(int i=0;i<size();i++) {
            if(getPageStatus(i).isError()) {
                return true;
            }
        }
        return false;
    }

    public boolean  containsWarning()
    {
        for(int i=0;i<size();i++) {
            if(getPageStatus(i).isWarning()) {
                return true;
            }
        }
        return false;
    }

    public String toString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{");
        for(int i=0;i<size();i++) {
            sb.append("[");
            sb.append(getPageStatus(i).toString());
            sb.append("]");
            if(i < size()-1) {
                sb.append(",");
            }
        }
        sb.append("}");
        return sb.toString();
    }

}



