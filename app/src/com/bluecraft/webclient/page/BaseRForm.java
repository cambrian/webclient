/*************************************************************************
 *  Copyright (c) 2002-2003, BlueCraft Software. 
 *  All Rights Reserved. No part of this program may be copied
 *  or used without the express written consent of BlueCraft Software.
 *************************************************************************/
// $Id: BaseRForm.java,v 1.2 2003/02/02 23:12:10 hyoon Exp $ 

package com.bluecraft.webclient.page;

import com.bluecraft.webclient.*;
import com.bluecraft.webclient.parser.*;
import com.bluecraft.webclient.mapping.*;

import com.bluecraft.common.Guid;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;


/**   
 * Form "Bean" interface to be used in <B>R</B>equest scope.
 *   
 * @author   <A href="mailto:hyoon@bluecraft.com">Hyoungsoo Yoon</A>
 * @version  $Revision: 1.2 $
 */   
public class BaseRForm
    extends BaseForm
    implements IRForm
{
    private boolean mIsInitialized = false;
    private HttpServletRequest mReq = null;  // cache it???
    
    public BaseRForm()
    {
        this(true);
    }

    public BaseRForm(boolean isInitialized)
    {
        super();
        mIsInitialized = isInitialized;
        if(mIsInitialized) {
            // set formBean....
        } else {
            //
        }
    }

    public boolean isInitialized()
    {
        return mIsInitialized;
    }

}



