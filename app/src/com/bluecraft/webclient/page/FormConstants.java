/*************************************************************************
 *  Copyright (c) 2002-2003, BlueCraft Software. 
 *  All Rights Reserved. No part of this program may be copied
 *  or used without the express written consent of BlueCraft Software.
 *************************************************************************/
// $Id: FormConstants.java,v 1.2 2003/02/02 23:12:10 hyoon Exp $ 

package com.bluecraft.webclient.page;


/**   
 *   
 * @author   <A href="mailto:hyoon@bluecraft.com">Hyoungsoo Yoon</A>
 * @version  $Revision: 1.2 $
 */   
public interface FormConstants
{
    static final String BCPARAM_FORM_PREFIX = "BC_Form_";
    static final String BCPARAM_FIELD_PREFIX = "BC_Field_";

    static final String BCPARAM_ANCHOR_ON_SUCCESS = "BC_AnchorSuccess";
    static final String BCPARAM_ANCHOR_ON_FAILURE = "BC_AnchorFailure";

}


