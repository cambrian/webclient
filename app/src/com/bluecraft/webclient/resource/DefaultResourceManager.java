/*************************************************************************
 *  Copyright (c) 2002-2003, BlueCraft Software. 
 *  All Rights Reserved. No part of this program may be copied
 *  or used without the express written consent of BlueCraft Software.
 *************************************************************************/
// $Id: DefaultResourceManager.java,v 1.2 2003/02/02 23:12:11 hyoon Exp $

package com.bluecraft.webclient.resource;

import com.bluecraft.webclient.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.ServletContext;
import java.util.logging.Logger;
import java.util.Locale;
import java.net.URL;
import java.util.Set;


/**
 *
 * @author   <A href="mailto:hyoon@bluecraft.com">Hyoungsoo Yoon</A>
 * @version  $Revision: 1.2 $
 */
public class DefaultResourceManager
    implements IResourceManager
{
    // Cache ServletContext
    private ServletContext mServletContext = null;

    private boolean mInitialized = false;


    protected DefaultResourceManager()
    {
        this(null); // ???
    }

    public DefaultResourceManager(ServletContext servletContext)
    {
        mServletContext = servletContext;
        // initialize(); // ....
    }

    public ServletContext getServletContext()
    {
        return mServletContext;
    }


    private void initialize(String resourceFilePath)
    {
        if(resourceFilePath != null) {
            //
        } else {
            //
        }

        // TODO:
        // initialize mime type list

        mInitialized = true;
    }



    public Locale getDefaultLocale()
    {
        return null;
    }

    public Locale getLocale(HttpServletRequest request)
    {
        return null;
    }

    public Locale[] getSupportedLocales()
    {
        return new Locale[0];
    }

    public String getMimeType(String file)
    {
        return null;
    }

}



