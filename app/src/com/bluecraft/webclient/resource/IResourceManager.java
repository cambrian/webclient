/*************************************************************************
 *  Copyright (c) 2002-2003, BlueCraft Software. 
 *  All Rights Reserved. No part of this program may be copied
 *  or used without the express written consent of BlueCraft Software.
 *************************************************************************/
// $Id: IResourceManager.java,v 1.2 2003/02/02 23:12:11 hyoon Exp $

package com.bluecraft.webclient.resource;

import com.bluecraft.webclient.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.ServletContext;
import java.util.Locale;
import java.net.URL;
import java.util.Set;


/**
 *
 * @author   <A href="mailto:hyoon@bluecraft.com">Hyoungsoo Yoon</A>
 * @version  $Revision: 1.2 $
 */
public interface IResourceManager
{
    Locale getDefaultLocale();
    Locale getLocale(HttpServletRequest request);
    Locale[] getSupportedLocales();

    String getMimeType(String file);

    //URL getResource(String path)
    //Set getResourcePaths(String path)

    ServletContext getServletContext();

}



