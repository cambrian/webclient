/*************************************************************************
 *  Copyright (c) 2002-2003, BlueCraft Software. 
 *  All Rights Reserved. No part of this program may be copied
 *  or used without the express written consent of BlueCraft Software.
 *************************************************************************/
// $Id: WebClientException.java,v 1.2 2003/02/02 23:12:10 hyoon Exp $ 

package com.bluecraft.webclient;


/**   
 * WebClientException is superclass of all Exceptions defined in WebClient.
 *
 * @author   <A href="mailto:hyoon@bluecraft.com">Hyoungsoo Yoon</A>
 * @version  $Revision: 1.2 $
 */   
public class WebClientException
    extends Exception
{
    /**
     * Constructs a new WebClientException.
     */
    public WebClientException()
    {
        super();
    }
    
    /**
     * Constructs a new WebClientException with the given message.
     *
     * @param message the detail message
     */
    public WebClientException(String message)
    {
        super(message);
    }

    /**
     * Constructs a new WebClientException with the specified detail message and cause. 
     *
     * @param message the detail message
     * @param cause the cause. null value means that the cause is nonexistent or unknown.
     */
    public WebClientException(String message, Throwable cause)
    {
        super(message, cause);
    }

    /**
     * Constructs a new WebClientException with the specified cause and a detail message of cause
     *
     * @param cause the cause. null value means that the cause is nonexistent or unknown.
     */
    public WebClientException(Throwable cause)
    {
        super(cause);
    }
        
}



