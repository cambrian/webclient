/*************************************************************************
 *  Copyright (c) 2002-2003, BlueCraft Software. 
 *  All Rights Reserved. No part of this program may be copied
 *  or used without the express written consent of BlueCraft Software.
 *************************************************************************/
// $Id: WebClientRuntimeException.java,v 1.2 2003/02/02 23:12:10 hyoon Exp $ 

package com.bluecraft.webclient;


/**   
 * WebClientRuntimeException is superclass of all RuntimeExceptions defined in WebClient.
 *
 * @author   <A href="mailto:hyoon@bluecraft.com">Hyoungsoo Yoon</A>
 * @version  $Revision: 1.2 $
 */   
public class WebClientRuntimeException
    extends RuntimeException
{
    /**
     * Constructs a new WebClientRuntimeException.
     */
    public WebClientRuntimeException()
    {
        super();
    }
    
    /**
     * Constructs a new WebClientRuntimeException with the given message.
     *
     * @param message the detail message
     */
    public WebClientRuntimeException(String message)
    {
        super(message);
    }

    /**
     * Constructs a new WebClientRuntimeException with the specified detail message and cause. 
     *
     * @param message the detail message
     * @param cause the cause. null value means that the cause is nonexistent or unknown.
     */
    public WebClientRuntimeException(String message, Throwable cause)
    {
        super(message, cause);
    }

    /**
     * Constructs a new WebClientRuntimeException with the specified cause and a detail message of cause
     *
     * @param cause the cause. null value means that the cause is nonexistent or unknown.
     */
    public WebClientRuntimeException(Throwable cause)
    {
        super(cause);
    }
        
}



