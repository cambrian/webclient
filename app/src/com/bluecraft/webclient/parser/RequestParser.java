/*************************************************************************
 *  Copyright (c) 2002-2003, BlueCraft Software. 
 *  All Rights Reserved. No part of this program may be copied
 *  or used without the express written consent of BlueCraft Software.
 *************************************************************************/
// $Id: RequestParser.java,v 1.2 2003/02/02 23:12:10 hyoon Exp $ 


package com.bluecraft.webclient.parser;

import com.bluecraft.webclient.*;

import javax.servlet.Servlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**   
 *   
 * @author   <A href="mailto:hyoon@bluecraft.com">Hyoungsoo Yoon</A>
 * @version  $Revision: 1.2 $
 */   
public abstract class RequestParser
    extends BaseNVPListParser
    implements IRequestParser
{
    private HttpServletRequest mRequest = null;


    public RequestParser(HttpServletRequest req)
    {
        super();
        setRequest(req);
        initialize();
    }

    public RequestParser(int sensitivity, HttpServletRequest req)
    {
        super(sensitivity);
        setRequest(req);
        initialize();
    }

    public RequestParser(String charEncoding, HttpServletRequest req)
    {
        super(charEncoding);
        setRequest(req);
        initialize();
    }

    public RequestParser(String charEncoding, int sensitivity, HttpServletRequest req)
    {
        super(charEncoding, sensitivity);
        setRequest(req);
        initialize();
    }

    
    public HttpServletRequest getRequest()
    {
        return mRequest;
    }

    private void setRequest(HttpServletRequest req)
    {
        mRequest = req;

        String charEnc = req.getCharacterEncoding();
        if(charEnc != null) {
            setCharacterEncoding(charEnc);
        }

        String contType = req.getHeader("Content-type");
        // ...
        
    }


    protected void initialize()
    {
        LogUtil.getLogger().entering("RequestParser", "initialize");

        //

        LogUtil.getLogger().exiting("RequestParser", "initialize");
    }

    
    
}




