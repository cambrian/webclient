/*************************************************************************
 *  Copyright (c) 2002-2003, BlueCraft Software. 
 *  All Rights Reserved. No part of this program may be copied
 *  or used without the express written consent of BlueCraft Software.
 *************************************************************************/
// $Id: BaseNVPListParser.java,v 1.2 2003/02/02 23:12:10 hyoon Exp $ 


package com.bluecraft.webclient.parser;

import com.bluecraft.webclient.*;

import java.util.*;
import java.text.*;


/**   
 *   
 * @author   <A href="mailto:hyoon@bluecraft.com">Hyoungsoo Yoon</A>
 * @version  $Revision: 1.2 $
 */   
public class BaseNVPListParser
    implements INVPListParser
{
    private int     mKeyCaseSensitivity;
    private Map     mMap = null;  // Map of (String, String[])
    private String  mCharEncoding = null;

    private static final String DEFAULT_CHAR_ENCODING = "US-ASCII";

    
    public BaseNVPListParser()
    {
        this(DEFAULT_CHAR_ENCODING);
    }

    public BaseNVPListParser(int sensitivity)
    {
        this(DEFAULT_CHAR_ENCODING, sensitivity);
    }

    public BaseNVPListParser(String charEncoding)
    {
        this(charEncoding, KEY_CASE_SENSITIVE);
    }

    public BaseNVPListParser(String charEncoding, int sensitivity)
    {
        mMap = new HashMap();
        mCharEncoding = charEncoding;
        mKeyCaseSensitivity = sensitivity;
    }


    private String toCanonicalKey(String key)
    {
        if(key == null) {
            return null; // ???
        }
        String trimmedKey = key.trim();
        if(trimmedKey.length() == 0) {
            return trimmedKey; // ???
        }
        switch(mKeyCaseSensitivity) {
        case KEY_CASE_SENSITIVE:
        default:
            return trimmedKey;
        case KEY_CASE_INSENSITIVE:
            return trimmedKey.toUpperCase();
        }
    }


    public int  getKeyCaseSensitivity()
    {
        return mKeyCaseSensitivity;
    }

    //private void setKeyCaseSensitivity(int sensitivity)
    //{
    //    mKeyCaseSensitivity = sensitivity;
    //}

    public String getCharacterEncoding()
    {
        return mCharEncoding;
    }

    protected void setCharacterEncoding(String s)
    {
        mCharEncoding = s;
    }


    public void clear()
    {
        mMap.clear();
    }

    public void add(String name)
    {
        add(name, new String[]{});
    }
    
    public void add(String name, String value)
    {
        add(name, new String[]{value});
    }

    public void add(String name, String[] values)
    {
        if(values == null) {
            return; // ???
        }
        String key = toCanonicalKey(name);
        synchronized(mMap) {
            if(mMap.containsKey(key)) {
                String[] oldValues = (String[]) mMap.get(key);
                String[] newValues = new String[oldValues.length + values.length];
                for(int i=0;i<oldValues.length;i++) {
                    newValues[i] = oldValues[i];
                }
                for(int j=0;j<values.length;j++) {
                    newValues[j+oldValues.length] = values[j];
                }
                mMap.put(key, newValues);
            } else {
                mMap.put(key, values);
            }
        }
    }

    public void set(String name)
    {
        set(name, new String[]{});
    }

    public void set(String name, String value)
    {
        set(name, new String[]{value});
    }

    public void set(String name, String[] values)
    {
        String key = toCanonicalKey(name);
        synchronized(mMap) {
            mMap.put(key, values);
        }
    }


    public String[] delete(String name)
    {
        String key = toCanonicalKey(name);
        return (String[]) mMap.remove(key);
    }

    public String[] remove(String name)
    {
        String key = toCanonicalKey(name);
        if(!mMap.containsKey(key)) {
            return null;
        }
        synchronized(mMap) {
            String[] values = (String[]) mMap.get(key);
            mMap.put(key, new String[]{});
            return values;
        }
    }

    public String  remove(String name, String value)
    {
        if(value == null) {
            return null; // ???
        }
        String key = toCanonicalKey(name);
        if(!mMap.containsKey(key)) {
            return null;
        }
        synchronized(mMap) {
            String[] oldValues = (String[]) mMap.get(key);
            int idx = -1;
            for(int k=0;k<oldValues.length;k++) {
                if(value.equals(oldValues[k])) {
                    idx = k;
                    break;
                }
            }
            if(idx == -1) {
                return null;
            }
            
            String[] newValues = new String[oldValues.length - 1];
            for(int i=0;i<idx;i++) {
                newValues[i] = oldValues[i];
            }
            for(int j=idx+1;j<oldValues.length;j++) {
                newValues[j-1] = oldValues[j];
            }
            mMap.put(key, newValues);

            return oldValues[idx];
        }
    }


    public boolean containsKey(String name)
    {
        String key = toCanonicalKey(name);
        return mMap.containsKey(key);
    }

    public String[] keys()
    {
        String[] keys = null;
        Object[] objs = mMap.keySet().toArray();
        if(objs != null) {
            int len = objs.length;
            keys = new String[len];
            for(int i=0;i<len;i++) {
                keys[i] = (String) objs[i];
            }
        }
        return keys;
    }


    public String getStringValue(String name)
    {
        return getStringValue(name, null);
    }

    public String getStringValue(String name, String defaultValue)
    {
        String[] values = getStringValues(name);
        if(values == null || values.length == 0) {
            return defaultValue;
        } else {
            return values[0];
        }
    }

    public String[] getStringValues(String name)
    {
        String key = toCanonicalKey(name);
        synchronized(mMap) {
            return (String[]) mMap.get(name);
        }        
    }


    public boolean getBooleanValue(String name)
    {
        return getBooleanValue(name, false);
    }

    public boolean getBooleanValue(String name, boolean defaultValue)
    {
        boolean[] values = getBooleanValues(name);
        if(values == null || values.length == 0) {
            return defaultValue;
        }
        return values[0];
    }

    public boolean[] getBooleanValues(String name)
    {
        if(name == null) {
            return null; // ???
        }
        String key = toCanonicalKey(name);
        if(!mMap.containsKey(key)) {
            return null;
        }
        synchronized(mMap) {
            String[] strValues = (String[]) mMap.get(key);
            boolean[] boolValues = new boolean[strValues.length];
            for(int k=0;k<strValues.length;k++) {
                boolValues[k] = Boolean.valueOf(strValues[k]).booleanValue();
            }
            return boolValues;
        }
    }


    public int getIntValue(String name)
    {
        return getIntValue(name, 0);
    }

    public int getIntValue(String name, int defaultValue)
    {
        int[] values = getIntValues(name);
        if(values == null || values.length == 0) {
            return defaultValue;
        }
        return values[0];
    }

    public int[] getIntValues(String name)
    {
        if(name == null) {
            return null; // ???
        }
        String key = toCanonicalKey(name);
        if(!mMap.containsKey(key)) {
            return null;
        }
        synchronized(mMap) {
            String[] strValues = (String[]) mMap.get(key);
            int[] intValues = new int[strValues.length];
            for(int k=0;k<strValues.length;k++) {
                try {
                    intValues[k] = Integer.parseInt(strValues[k]);
                } catch(NumberFormatException nfx) {
                    intValues[k] = 0;
                }
            }
            return intValues;
        }
    }


    public double getDoubleValue(String name)
    {
        return getDoubleValue(name, 0.0);
    }

    public double getDoubleValue(String name, double defaultValue)
    {
        double[] values = getDoubleValues(name);
        if(values == null || values.length == 0) {
            return defaultValue;
        }
        return values[0];
    }

    public double[] getDoubleValues(String name)
    {
        if(name == null) {
            return null; // ???
        }
        String key = toCanonicalKey(name);
        if(!mMap.containsKey(key)) {
            return null;
        }
        synchronized(mMap) {
            String[] strValues = (String[]) mMap.get(key);
            double[] doubleValues = new double[strValues.length];
            for(int k=0;k<strValues.length;k++) {
                try {
                    doubleValues[k] = Double.parseDouble(strValues[k]);
                } catch(NumberFormatException nfx) {
                    doubleValues[k] = 0.0;
                }
            }
            return doubleValues;
        }
    }


    public Date getDateValue(String name)
    {
        //
        return new Date();
    }

    public Date getDateValue(String name, DateFormat df)
    {
        //
        return new Date();
    }

    public Date getDateValue(String name, Date defaultValue)
    {
        //
        return new Date();
    }

    public Date getDateValue(String name, DateFormat df, Date defaultValue)
    {
        //
        return new Date();
    }

    public Date[] getDateValues(String name)
    {
        //
        return null;
    }

    public Date[] getDateValues(String name, DateFormat df)
    {
        //
        return null;
    }


    public String toString()
    {
        StringBuffer sb = new StringBuffer();
        if(mMap != null) {
            sb.append("{");

            Set kset = mMap.keySet();
            Iterator kit = kset.iterator();
            while(kit.hasNext()) {
                String key = (String) kit.next();
                sb.append(key);
                sb.append("=");
                String[] values = (String[]) mMap.get(key);
                if(values != null) {
                    sb.append("[");
                    for(int i=0;i<values.length;i++) {
                        sb.append(values[i]);
                        if(i != values.length-1) {
                            sb.append(",");
                        }
                    }
                    sb.append("]");
                }
                if(kit.hasNext()) {
                    sb.append(", ");
                }
            }

            sb.append("}");
        }
        return sb.toString();
    }
    
}



