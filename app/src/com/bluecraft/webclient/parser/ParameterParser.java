/*************************************************************************
 *  Copyright (c) 2002-2003, BlueCraft Software. 
 *  All Rights Reserved. No part of this program may be copied
 *  or used without the express written consent of BlueCraft Software.
 *************************************************************************/
// $Id: ParameterParser.java,v 1.2 2003/02/02 23:12:10 hyoon Exp $ 


package com.bluecraft.webclient.parser;

import com.bluecraft.webclient.*;

import javax.servlet.Servlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.*;


/**   
 *   
 * @author   <A href="mailto:hyoon@bluecraft.com">Hyoungsoo Yoon</A>
 * @version  $Revision: 1.2 $
 */   
public class ParameterParser
    extends RequestParser
    implements IParameterParser
{
    //private MultipartStreamReader mMSR;
    
    
    public ParameterParser(HttpServletRequest req)
    {
        super(req);
    }

    public ParameterParser(int sensitivity, HttpServletRequest req)
    {
        super(sensitivity, req);
    }

    public ParameterParser(String charEncoding, HttpServletRequest req)
    {
        super(charEncoding, req);
    }

    public ParameterParser(String charEncoding, int sensitivity, HttpServletRequest req)
    {
        super(charEncoding, sensitivity, req);
    }


    protected void initialize()
    {
        LogUtil.getLogger().entering("ParameterParser", "initialize");

        // ???
        Enumeration names = getRequest().getParameterNames();
        if ( names != null )
        {
            while(names.hasMoreElements())
            {
                String tmp = (String) names.nextElement();
                add(tmp, (String[]) getRequest().getParameterValues(tmp)); // ???
            }
        }


        if (isMultipartRequest()) 
        {
            MultipartStreamReader msr = new MultipartStreamReader(getRequest());
            try {
                parseMultipartRequest(msr);
            } catch(Exception ex) {
                ex.printStackTrace();
            }
        }
        

        LogUtil.getLogger().exiting("ParameterParser", "initialize");
    }


    protected boolean isMultipartRequest()
    {
        String contentType = getRequest().getHeader("Content-type");
        if (contentType != null && contentType.startsWith("multipart/form-data")) {
            return true;
        } else {
            return false;
        }
    }


    /**
     * Gets the boundary marker, moves past the first boundary,
     * and calls processParts() to process each part of the
     * request.
     *
     * @exception Exception
     */
    public void parseMultipartRequest(MultipartStreamReader msr)
        throws Exception
    {
        processMultiparts(msr);
    }


    /**
     * Processes each part of the multipart/form-data request.
     *
     * @exception Exception
     */
    private void processMultiparts(MultipartStreamReader msr)
        throws Exception
    {
        LogUtil.getLogger().entering("ParameterParser", "processMultiparts");

        while(msr.hasNextDisposition())
        {
            ContentDisposition disposition = msr.getNextDisposition();
            String name = disposition.getName();
            if(disposition.isFile()) {
                // ???
                //add(name, msr.getUploadedFile(disposition));
            } else {
                // ???
                //add(name, msr.getVariable(disposition));
            }

        }

        LogUtil.getLogger().exiting("ParameterParser", "processMultiparts");
    }

    
}




