/*************************************************************************
 *  Copyright (c) 2002-2003, BlueCraft Software. 
 *  All Rights Reserved. No part of this program may be copied
 *  or used without the express written consent of BlueCraft Software.
 *************************************************************************/
// $Id: CookieParser.java,v 1.2 2003/02/02 23:12:10 hyoon Exp $ 


package com.bluecraft.webclient.parser;

import com.bluecraft.webclient.*;

import javax.servlet.Servlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Cookie;


/**   
 *   
 * @author   <A href="mailto:hyoon@bluecraft.com">Hyoungsoo Yoon</A>
 * @version  $Revision: 1.2 $
 */   
public class CookieParser
    extends RequestParser
    implements ICookieParser
{
    
    public CookieParser(HttpServletRequest req)
    {
        super(req);
    }

    public CookieParser(int sensitivity, HttpServletRequest req)
    {
        super(sensitivity, req);
    }

    public CookieParser(String charEncoding, HttpServletRequest req)
    {
        super(charEncoding, req);
    }

    public CookieParser(String charEncoding, int sensitivity, HttpServletRequest req)
    {
        super(charEncoding, sensitivity, req);
    }


    protected void initialize()
    {
        LogUtil.getLogger().entering("CookieParser", "initialize");

        Cookie[] cookies = getRequest().getCookies();
        if(cookies != null) {
            LogUtil.getLogger().fine("Number of Cookies "+cookies.length);
    
            for (int i=0; i<cookies.length; i++)
            {
                String name = cookies[i].getName();
                String value = cookies[i].getValue();
                //LogUtil.getLogger().finer("Adding "+name+"="+value);
                if(value == null) {
                    // Do we need this???
                    value = "";
                }
                add(name,value);
            }
        }
        
        LogUtil.getLogger().exiting("CookieParser", "initialize");
    }

    
}



