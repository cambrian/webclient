/*************************************************************************
 *  Copyright (c) 2002-2003, BlueCraft Software. 
 *  All Rights Reserved. No part of this program may be copied
 *  or used without the express written consent of BlueCraft Software.
 *************************************************************************/
// $Id: ContentDisposition.java,v 1.2 2003/02/02 23:12:10 hyoon Exp $ 


package com.bluecraft.webclient.parser;

import com.bluecraft.webclient.*;

import javax.servlet.Servlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**   
 * ContentDisposition is an inner class that encapsulates the
 * properties of the Content-Disposition HTTP header.
 * Specifically, this class stores the name of the web variable
 * and the filename.
 *   
 * @author   <A href="mailto:hyoon@bluecraft.com">Hyoungsoo Yoon</A>
 * @version  $Revision: 1.2 $
 */   
public  class ContentDisposition
{
    private String name = "";
    private String filename = "";


    /**
     * Returns the name of the web variable extracted from the
     * Content-Disposition header.
     *
     * @return Name of web variable
     */
    public String getName()
    {
        return name;
    }


    /**
     * Sets the name of the web variable.
     *
     * @param name String containing the name of the web
     *  variable
     */
    public void setName(String name)
    {
        this.name = name;
    }


    /**
     * Returns the filename extracted from the Content-
     * Disposition header.
     *
     * @return String containing the filename
     */
    public String getFilename()
    {
        return filename;
    }


    /**
     * Sets the filename value.
     *
     * @param filename String containing the name of the file
     */
    public void setFilename(String filename)
    {
        this.filename = filename;
    }

    public boolean isFile()
    {
        String fileName = getFilename();
        if(fileName != null && !fileName.equals("")) {
            return true;
        } else {
            return false;
        }
    }
    

}



