/*************************************************************************
 *  Copyright (c) 2002-2003, BlueCraft Software. 
 *  All Rights Reserved. No part of this program may be copied
 *  or used without the express written consent of BlueCraft Software.
 *************************************************************************/
// $Id: MultipartStreamReader.java,v 1.2 2003/02/02 23:12:10 hyoon Exp $ 


package com.bluecraft.webclient.parser;

import com.bluecraft.webclient.*;

import java.io.IOException;
import java.io.InputStream;

import javax.servlet.Servlet;
import javax.servlet.ServletRequest;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.*;


/**   
 * MultipartStreamReader is an inner class that contains a
 * single readLine() method. This method returns a single line
 * from the request input stream.
 *   
 * @author   <A href="mailto:hyoon@bluecraft.com">Hyoungsoo Yoon</A>
 * @version  $Revision: 1.2 $
 */   
public class MultipartStreamReader
{
    private ServletInputStream mSIS = null;
    private ServletRequest mRequest = null;
    private String mBoundaryMarker = null;

    private boolean mLookedAhead = false;
    private ContentDisposition mNextDisposition = null;

    private static final int BUFFER_SIZE = 1024; // ...

    
    public MultipartStreamReader(ServletRequest request)
    {
        mRequest = request;
        try {
            mSIS = request.getInputStream();
        } catch(IOException iox) {
            iox.printStackTrace();
        }
    }
    
    /**
     * Reads a single line (up to 1K in length) from the input
     * stream. The line is limited to 1K in length since this
     * method is for reading HTTP headers and web variable
     * values, not files.
     *
     * @return String containing a single line from the input stream
     * @exception IOException
     */
    public String readLine()
        throws IOException
    {
        String line = "";

        byte[] bytes = new byte[1024]; //1K buffer

        int bytesRead = mSIS.readLine(bytes, 0, BUFFER_SIZE);

        if (bytesRead == -1)
        {
            return null; //no line to read so return null
        }
        else
        {
            //convert byte array to a string
            line = new String(bytes, 0, bytesRead, "ISO8859_1");
        }

        if (line.endsWith("\r\n"))
        {
            //remove the trailing \r\n
            line = line.substring(0, line.length()-2);
        }

        return line;
    }



    /**
     * Returns the boundary marker from the Content-Type header.
     *
     * @return String containing the boundary marker
     */
    private String getBoundaryMarker()
    {
        if(mBoundaryMarker == null) {
            String contentType = mRequest.getContentType();
            String boundaryMarker = "boundary=";
            int boundaryIndex = contentType.indexOf(boundaryMarker) + boundaryMarker.length();
            mBoundaryMarker = "--" + contentType.substring(boundaryIndex);
        }
        return mBoundaryMarker;
    }


    /**
     * Moves past the next empty line in the input stream.
     *
     * @exception IOException
     */
    private void findEmptyLine()
        throws IOException
    {
        String line = readLine();

        while (line != null && !line.equals(""))
        {
            line = readLine();
        }
    }


    /**
     * Moves past the next boundary marker.
     *
     * @exception IOException
     */
    private void findNextBoundary()
        throws IOException
    {
        String line = readLine();
        while (line != null && !line.startsWith(getBoundaryMarker()))
        {
            line = readLine();
        }
    }


    public boolean hasNextDisposition()
    {
        if(mLookedAhead == false) {
            try {
                findNextBoundary();
                mNextDisposition = getDisposition();
            } catch(Exception ex) {
                ex.printStackTrace();
            }
            mLookedAhead = true;
        }
        if(mNextDisposition != null) {
            return true;
        } else {
            return false;
        }
    }

    public ContentDisposition getNextDisposition()
    {
        if(mLookedAhead == false) {
            try {
                findNextBoundary();
                mNextDisposition = getDisposition();
            } catch(Exception ex) {
                ex.printStackTrace();
            }
            mLookedAhead = true;
        }
        return mNextDisposition;
    }


    /**
     * Reads the Content-Disposition header and stores it in a new
     * ContentDisposition object.
     *
     * @return ContentDisposition object
     * @exception Exception
     */
    private ContentDisposition getDisposition()
        throws Exception
    {
        //read first line of this part of the multipart request
        String line = readLine();

        while (line != null && !line.equals("") &&
               !line.toLowerCase().startsWith("content-disposition:"))
        {
            line = readLine();
        }

        ContentDisposition disposition = new ContentDisposition();

        if (line == null || !line.toLowerCase().startsWith("content-disposition:"))
        {
            return null;
        }

        String name = getDispositionName(line);
        disposition.setName(name);
        String filename = getDispositionFilename(line);
        disposition.setFilename(filename);

        return disposition;
    }


    /**
     * Returns the web variable name from the Content-Disposition
     * header.
     *
     * @param line String containing the Content-Disposition header
     * @return String containing the name of the web variable
     */
    private String getDispositionName(String line)
    {
        String search = "name=\"";
        int nameIndex = line.toLowerCase().indexOf(search);

        if (nameIndex == -1)
        {
            return "";
        }

        nameIndex += search.length();
        String dispositionName = line.substring(nameIndex, line.indexOf("\"", nameIndex+1));

        return dispositionName;
    }


    /**
     * Returns the filename from the Content-Disposition header.
     *
     * @param line String containing the Content-Disposition header
     * @return String containing the filename if it exists
     */
    private String getDispositionFilename(String line)
    {
        String fileSeparator = "/"; // temporary
        String search = "filename=\"";
        int filenameIndex = line.toLowerCase().indexOf(search);

        if (filenameIndex == -1)
        {
            return "";
        }

        filenameIndex += search.length();
        String filename = line.substring(filenameIndex, line.indexOf("\"", filenameIndex));
        filename = filename.substring(filename.lastIndexOf(fileSeparator)+1);

        return filename;
    }

    

    public String getVariable(ContentDisposition disposition)
        throws IOException
    {
        findEmptyLine(); //move past empty line
        String val = readLine();
        findNextBoundary(); //move past next boundary
        return val;
    }

    public UploadedFile getUploadedFile(ContentDisposition disposition)
        throws IOException
    {
        String saveDirectory = "aaa/zzz/"; // ....
        String filePath = saveDirectory + disposition.getFilename();
        storeFile(filePath);
        return new UploadedFile(filePath);
    }


    /**
     * Reads a file from the multipart/form-data request and writes
     * it to the file system.
     *
     * @param disposition ContentDisposition object containing the
     *  name of the web variable and filename
     */
    private void storeFile(String filePath)
        throws IOException
    {
        FileOutputStream fileOut = null;

        try
        {
            //create file object and output stream for writing file
            File file = new File(filePath);
            fileOut = new FileOutputStream(file);

            findEmptyLine(); //move past empty line

            int bytesRead = 0;

            boolean rnAddFlag = false;

            byte[] bytes = new byte[BUFFER_SIZE];

            //read entire upload file from request in 8K chunks
            while ((bytesRead = mSIS.readLine(bytes, 0, BUFFER_SIZE))
                   != -1)
            {
                //pre-check for boundary marker
                if (bytes[0] == '-' && bytes[1] == '-' && bytes[2] == '-'
                    && bytesRead < 500)
                {
                    //this may be boundary marker, get string to make sure
                    String line = new String(bytes, 0, bytesRead,
                                             "ISO8859_1");

                    if (line.startsWith(mBoundaryMarker))
                    {
                        //we're at the boundary marker so we're done reading
                        break;
                    }
                }

                //we read a \r\n from the previous iteration that needs
                //to be written to the file
                if (rnAddFlag)
                {
                    fileOut.write('\r');
                    fileOut.write('\n');
                    rnAddFlag = false;
                }

                //since ServletInputStream adds its own \r\n to the last
                //line read, don't write it to the file until we're sure
                //that this is not the last line
                if (bytesRead > 2 && bytes[bytesRead-2] == '\r' &&
                    bytes[bytesRead-1] == '\n')
                {
                    bytesRead = bytesRead - 2;
                    rnAddFlag = true;
                }

                fileOut.write(bytes, 0, bytesRead); //write data to file
            }
        }
        finally
        {
            fileOut.close();
        }
    }





}




