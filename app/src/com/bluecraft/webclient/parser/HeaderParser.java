/*************************************************************************
 *  Copyright (c) 2002-2003, BlueCraft Software. 
 *  All Rights Reserved. No part of this program may be copied
 *  or used without the express written consent of BlueCraft Software.
 *************************************************************************/
// $Id: HeaderParser.java,v 1.2 2003/02/02 23:12:10 hyoon Exp $ 


package com.bluecraft.webclient.parser;

import com.bluecraft.webclient.*;

import javax.servlet.Servlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Cookie;

import java.util.Enumeration;


/**   
 *   
 * @author   <A href="mailto:hyoon@bluecraft.com">Hyoungsoo Yoon</A>
 * @version  $Revision: 1.2 $
 */   
public class HeaderParser
    extends RequestParser
    implements IHeaderParser
{
    
    public HeaderParser(HttpServletRequest req)
    {
        super(req);
    }

    public HeaderParser(int sensitivity, HttpServletRequest req)
    {
        super(sensitivity, req);
    }

    public HeaderParser(String charEncoding, HttpServletRequest req)
    {
        super(charEncoding, req);
    }

    public HeaderParser(String charEncoding, int sensitivity, HttpServletRequest req)
    {
        super(charEncoding, sensitivity, req);
    }


    protected void initialize()
    {
        LogUtil.getLogger().entering("HeaderParser", "initialize");

        Enumeration names = getRequest().getHeaderNames();
        while(names.hasMoreElements()) {
            String name = (String) names.nextElement();
            // TODO: If name==cookie, skip it???
            Enumeration values = getRequest().getHeaders(name);
            if(values == null || !values.hasMoreElements()) {
                // Do we need this???
                add(name, new String[]{});
            } else {
                while(values.hasMoreElements()) {
                    String value = (String) values.nextElement();
                    add(name,value);
                }
            }
        }

        LogUtil.getLogger().exiting("HeaderParser", "initialize");
    }

    
}



