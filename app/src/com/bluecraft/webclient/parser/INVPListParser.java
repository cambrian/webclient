/*************************************************************************
 *  Copyright (c) 2002-2003, BlueCraft Software. 
 *  All Rights Reserved. No part of this program may be copied
 *  or used without the express written consent of BlueCraft Software.
 *************************************************************************/
// $Id: INVPListParser.java,v 1.2 2003/02/02 23:12:10 hyoon Exp $ 


package com.bluecraft.webclient.parser;

import com.bluecraft.webclient.*;

import javax.servlet.Servlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.util.Date;
import java.util.Enumeration;

/**   
 *   
 * @author   <A href="mailto:hyoon@bluecraft.com">Hyoungsoo Yoon</A>
 * @version  $Revision: 1.2 $
 */   
public interface INVPListParser
{
    static final int KEY_CASE_SENSITIVE = 0;
    static final int KEY_CASE_INSENSITIVE = 1;

    String getCharacterEncoding ();
    //void setCharacterEncoding (String s);

    int  getKeyCaseSensitivity();
    //void setKeyCaseSensitivity(int sensitivity);

    void clear();
    void add(String name);
    void add(String name, String value);
    void add(String name, String[] values);
    void set(String name);
    void set(String name, String value);
    void set(String name, String[] values);
    String[]  delete(String name); // delete key and values
    String[]  remove(String name); // delete values only
    String    remove(String name, String value);

    boolean   containsKey(String name);
    String[]  keys();

    String    getStringValue(String name);
    String    getStringValue(String name, String defaultValue);
    String[]  getStringValues(String name);
    
    boolean   getBooleanValue(String name);
    boolean   getBooleanValue(String name, boolean defaultValue);
    boolean[] getBooleanValues(String name);
    
    int getIntValue(String name);
    int getIntValue(String name, int defaultValue);
    int[] getIntValues(String name);
    
    //long getLongValue(String name);
    //long getLongValue(String name, long defaultValue);
    //long[] getLongValues(String name);
    
    //float getFloatValue(String name);
    //float getFloatValue(String name, float defaultValue);
    //float[] getFloatValues(String name);
    
    double getDoubleValue(String name);
    double getDoubleValue(String name, double defaultValue);
    double[] getDoubleValues(String name);
    
    //BigDecimal getBigDecimalValue(String name);
    //BigDecimal getBigDecimalValue(String name, BigDecimal defaultValue);
    //BigDecimal[] getBigDecimalValues(String name);
    
    Date getDateValue(String name);
    Date getDateValue(String name, DateFormat df);
    Date getDateValue(String name, Date defaultValue);
    Date getDateValue(String name, DateFormat df, Date defaultValue);
    Date[] getDateValues(String name);
    Date[] getDateValues(String name, DateFormat df);
    
    
}



