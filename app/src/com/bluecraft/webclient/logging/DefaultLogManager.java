/*************************************************************************
 *  Copyright (c) 2002-2003, BlueCraft Software. 
 *  All Rights Reserved. No part of this program may be copied
 *  or used without the express written consent of BlueCraft Software.
 *************************************************************************/
// $Id: DefaultLogManager.java,v 1.2 2003/02/02 23:12:10 hyoon Exp $ 

package com.bluecraft.webclient.logging;

import com.bluecraft.webclient.*;
import com.bluecraft.webclient.config.*;
import com.bluecraft.webclient.processor.*;
import com.bluecraft.webclient.session.*;
import com.bluecraft.webclient.resource.*;
import com.bluecraft.webclient.util.*;

import java.io.InputStream;
import java.io.IOException;
import java.net.URL;

import java.util.logging.Logger;


/**   
 * Utility class for Java logging framework.
 *
 * @author   <A href="mailto:hyoon@bluecraft.com">Hyoungsoo Yoon</A>
 * @version  $Revision: 1.2 $
 */
public class DefaultLogManager
    implements ILogManager
{
    private static DefaultLogManager mLogManager = null;

    private DefaultLogManager()
    {
        //
    }

    public static DefaultLogManager getInstance()
    {
        if(mLogManager != null) {
            mLogManager = new DefaultLogManager();
        }
        return mLogManager;
    }
    
    public Logger getLogger()
    {
        return Logger.getAnonymousLogger();
    }

}

