/*************************************************************************
 *  Copyright (c) 2002-2003, BlueCraft Software. 
 *  All Rights Reserved. No part of this program may be copied
 *  or used without the express written consent of BlueCraft Software.
 *************************************************************************/
// $Id: DefaultConfigManager.java,v 1.2 2003/02/02 23:12:10 hyoon Exp $

package com.bluecraft.webclient.config;

import com.bluecraft.webclient.*;

import javax.servlet.ServletContext;
import java.util.prefs.Preferences;
import java.util.logging.Logger;
import java.util.List;


/**
 *
 * @author   <A href="mailto:hyoon@bluecraft.com">Hyoungsoo Yoon</A>
 * @version  $Revision: 1.2 $
 */
public class DefaultConfigManager
    implements IConfigManager
{
    // Cache ServletContext
    private ServletContext mServletContext = null;

    private IConfig  mAppConfig = null;
    private List     mConfigList = null;  // excludes the AppConfig....

    private boolean mInitialized = false;


    protected DefaultConfigManager()
    {
        this(null); // ???
    }

    public DefaultConfigManager(ServletContext servletContext)
    {
        mServletContext = servletContext;
        // initialize();  // ....
    }

    public ServletContext getServletContext()
    {
        return mServletContext;
    }


    public void initialize(String configFilePath)
    {
        if(configFilePath != null) {
            //
        } else {
            //
        }

        // Read configuration for web-app
        //String configFile = getServletContext().getInitParameter("CONFIG_FILE_PATH");
        //LogUtil.getLogger().fine("CONFIG_FILE_PATH = " + configFile);
        //initialize(configFile);

        mInitialized = true;
    }

    public IConfig getAppConfig()
    {
        return mAppConfig;
    }

    public IConfig[] getConfigs()
    {
        // TODO:
        return (IConfig[]) mConfigList.toArray();
    }

    public void addConfig(IConfig config)
    {
        if(mAppConfig == null && config.isAppConfig() == true) {
            mAppConfig = config;
        } else {
            mConfigList.add(config);
        }

        refreshRelationship(config);
    }

    /**
     * @todo
     * @param config
     */
    private void refreshRelationship(IConfig config)
    {
        // TODO:
        if(mAppConfig != null) {
            String appConfigPath = mAppConfig.getConfigPath();
            String configPath = config.getConfigPath();
            // ...
            //
        }

        // for .... mConfigList...
        //
    }

}



