/*************************************************************************
 *  Copyright (c) 2002-2003, BlueCraft Software. 
 *  All Rights Reserved. No part of this program may be copied
 *  or used without the express written consent of BlueCraft Software.
 *************************************************************************/
// $Id: BaseConfig.java,v 1.2 2003/02/02 23:12:10 hyoon Exp $

package com.bluecraft.webclient.config;

import com.bluecraft.webclient.*;

import javax.servlet.ServletConfig;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.prefs.BackingStoreException;
import java.util.prefs.InvalidPreferencesFormatException;
import java.util.prefs.Preferences;


/**
 *
 * @author   <A href="mailto:hyoon@bluecraft.com">Hyoungsoo Yoon</A>
 * @version  $Revision: 1.2 $
 */
public class BaseConfig
    implements BaseConfigKeys, IConfig, Serializable
{
    // Cache ServerConfig
    private ServletConfig mServletConfig = null;

    protected String mConfigPath = null; // "/aaa/bbb";
    protected Preferences mPrefs = null;
    protected boolean mConfigured = false;
    protected boolean mIsAppConfig = false;
    private IConfig mParent = null;
    private List    mChildren = new ArrayList();

    public BaseConfig()
    {
        this(null); // ???
    }

    public BaseConfig(ServletConfig servletConfig)
    {
        mServletConfig = servletConfig;
        initConfig();
    }

    public ServletConfig getServletConfig()
    {
        return mServletConfig;
    }

    public boolean isAppConfig()
    {
        //return (this instanceof AppConfig);
        return mIsAppConfig;
    }
    public String getConfigPath()
    {
        return mConfigPath;
    }

    public Preferences getPreferences()
    {
        return mPrefs;
    }

    protected void initConfig()
    {
        mIsAppConfig = false;

        initPreferences();
    }

    protected void initPreferences()
    {
        mConfigPath = getServletConfig().getInitParameter(BASE_CONFIG_PATH);
        LogUtil.getLogger().fine("BASE_CONFIG_PATH = " + mConfigPath);

        String configFileName = getServletConfig().getInitParameter(CONFIG_FILE_NAME);
        LogUtil.getLogger().fine("CONFIG_FILE_NAME = " + configFileName);

        if(!mConfigured) {
            synchronized(mPrefs) {
                importPreferences(configFileName);
                //mPrefs = Preferences.userNodeForPackage(this.getClass());
                Preferences rootPrefs = Preferences.systemRoot();
                mPrefs = rootPrefs.node(mConfigPath);
                try {
                    mPrefs.flush();
                } catch (BackingStoreException e) {
                    LogUtil.getLogger().warning("Unable to write to backing store: " + e);
                }
                mConfigured = true;
            }
        }
    }


    protected void printPreferences()
    {
        try {
            String keys[] = mPrefs.keys();
            for (int i=0, n=keys.length; i < n; i++) {
                LogUtil.getLogger().fine(keys[i] + ": " + mPrefs.get(keys[i], "Unknown"));
            }
        } catch (BackingStoreException e) {
            LogUtil.getLogger().warning("Unable to read backing store: " + e);
        }
    }


    public void storePreference(String key, String value)
    {
        mPrefs.put(key, value);
    }

    public void storePreferences()
    {
        //...

    }


    public void importPreferences(String fileName)
    {
        try {
            InputStream is =
                new BufferedInputStream(
                    new FileInputStream(fileName));
            Preferences.importPreferences(is);
            is.close();
        } catch(InvalidPreferencesFormatException ipfEx) {
            // ignore
        } catch(IOException ioEx) {
            // ignore as well
        }
    }


    public void exportPreferences(String fileName)
    {
        try {
            mPrefs.flush();
            OutputStream osTree =
                new BufferedOutputStream(
                    new FileOutputStream(fileName));
            mPrefs.exportSubtree(osTree);
            osTree.close();
        } catch(IOException ioEx) {
            // ignore
        } catch(BackingStoreException bsEx) {
            // ignore too
        }
    }


    public void cleanUp()
    {
        try {
            mPrefs.removeNode();
        } catch (BackingStoreException e) {
            LogUtil.getLogger().warning("Unable to access backing store: " + e);
        }
    }

    //
    public IConfig getParent()
    {
        return mParent;
    }

    public void setParent(IConfig config)
    {
        mParent = config;
    }

    public IConfig[] getChildren()
    {
        // TODO:
        return (IConfig[]) mChildren.toArray();
    }

    public void addChild(IConfig config)
    {
        mChildren.add(config);
    }

    public void setChildren(IConfig[] configs)
    {
        mChildren.clear();
        mChildren.addAll(Arrays.asList(configs));
    }

}



