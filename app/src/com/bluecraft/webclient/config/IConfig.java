/*************************************************************************
 *  Copyright (c) 2002-2003, BlueCraft Software. 
 *  All Rights Reserved. No part of this program may be copied
 *  or used without the express written consent of BlueCraft Software.
 *************************************************************************/
// $Id: IConfig.java,v 1.2 2003/02/02 23:12:10 hyoon Exp $

package com.bluecraft.webclient.config;

import com.bluecraft.webclient.*;

import java.util.prefs.Preferences;


/**
 *
 * @author   <A href="mailto:hyoon@bluecraft.com">Hyoungsoo Yoon</A>
 * @version  $Revision: 1.2 $
 */
public interface IConfig
{
    static final String BASE_CONFIG_PATH = "BaseConfigPath";
    static final String CONFIG_FILE_NAME = "ConfigFileName";

    boolean     isAppConfig();
    String      getConfigPath();

    //
    IConfig     getParent();
    IConfig[]   getChildren();

    Preferences getPreferences();

}



