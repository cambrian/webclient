/*************************************************************************
 *  Copyright (c) 2002-2003, BlueCraft Software. 
 *  All Rights Reserved. No part of this program may be copied
 *  or used without the express written consent of BlueCraft Software.
 *************************************************************************/
// $Id: IConfigManager.java,v 1.2 2003/02/02 23:12:10 hyoon Exp $

package com.bluecraft.webclient.config;

import com.bluecraft.webclient.*;

import javax.servlet.ServletContext;


/**
 *
 * @author   <A href="mailto:hyoon@bluecraft.com">Hyoungsoo Yoon</A>
 * @version  $Revision: 1.2 $
 */
public interface IConfigManager
{
    IConfig    getAppConfig();
    IConfig[]  getConfigs();  // excludes the AppConfig....

    ServletContext getServletContext();

    void addConfig(IConfig config);

    //IConfig     getParent(IConfig config);
    //IConfig[]   getChildren(IConfig config);

}



