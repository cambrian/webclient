/*************************************************************************
 *  Copyright (c) 2002-2003, BlueCraft Software. 
 *  All Rights Reserved. No part of this program may be copied
 *  or used without the express written consent of BlueCraft Software.
 *************************************************************************/
// $Id: BaseServlet.java,v 1.2 2003/02/02 23:12:10 hyoon Exp $

package com.bluecraft.webclient;

import com.bluecraft.webclient.config.*;
import com.bluecraft.webclient.processor.*;
import com.bluecraft.webclient.session.*;
import com.bluecraft.webclient.resource.*;
import com.bluecraft.webclient.util.*;
import com.bluecraft.common.Guid;

import java.io.InputStream;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.MissingResourceException;
import javax.servlet.ServletContext;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.UnavailableException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;
import java.util.logging.Logger;
import java.util.logging.Level;


/**
 *
 * @author   <A href="mailto:hyoon@bluecraft.com">Hyoungsoo Yoon</A>
 * @version  $Revision: 1.2 $
 */
public abstract class BaseServlet
    extends HttpServlet
    //implements IServlet, IConfig, BaseServletConstants
    implements IServlet, BaseServletConstants
{
    //
    protected IApplicationManager mApplicationManager = null;

    //
    //protected IConfigManager mConfigManager = null;

    //
    //protected IResourceManager mResourceManager = null;

    //
    protected HeartBeat mHeartBeat = null;

    //
    protected ISessionManager  mSessionManager = null;

    //
    private IProcessor  mAppProcessor = null;


    // Server start-up time.
    // This is initialized in init().
    // This serves as the lower bound for lastModified values.
    private static long s_baseServletStarted = 0L;


    /**
     * The URL pattern to which we are mapped in our web application
     * deployment descriptor.  FIXME - multiples???
     */
    //protected URLPattern mServletURLPattern = null;
    protected List mServletURLPattern = new ArrayList();


    /**
     * The servlet name under which we are registered in our web application
     * deployment descriptor.
     */
    protected String mServletName = null;


    /**
     * Remember a servlet mapping from our web application deployment
     * descriptor, if it is for this servlet.
     *
     * @param servletName The name of the servlet being mapped
     * @param urlPattern The URL pattern to which this servlet is mapped
     */
    public void addServletMapping(String servletName, String urlPattern)
    {
        if (servletName == null) {
            return;
        }
        if (servletName.equals(mServletName)) {
            mServletURLPattern.add(new URLPattern(urlPattern));
        }
    }

    private void initServlet()
        throws ServletException
    {
        LogUtil.getLogger().entering("BaseServlet","initServlet");

        ServletContext servletContext = getServletContext();
        IHostManager hostManager = DefaultHostManager.getInstance();
        mApplicationManager = hostManager.getApplicationManager(servletContext);
        LogUtil.getLogger().fine("mApplicationManager = " + mApplicationManager);

        // Remember our servlet name
        mServletName = getServletName();
        LogUtil.getLogger().info("Servlet Name = " + mServletName);
        // Register it
        mApplicationManager.registerServlet(this);


        //java.util.Enumeration getInitParameterNames()

        //java.lang.String getServletContextName()


        /*
        // Get configManager.
        mConfigManager = DefaultConfigManager.getInstance();

        // Read configuration
        String configFile = getServletConfig().getInitParameter("CONFIG_FILE_PATH");
        LogUtil.getLogger().fine("CONFIG_FILE_PATH = " + configFile);

        ((DefaultConfigManager) mConfigManager).initialize(configFile);
        */

        // Get resourceManager.
        //mResourceManager = DefaultResourceManager.getInstance();


        // AppContext??


        // Initialize DB
        // ???



        // Initialize Session management
        mSessionManager = DefaultSessionManager.getInstance();


        // Initialize RequestProcessor
        if(mAppProcessor == null) {
            mAppProcessor = initializeProcessor();
        }


        // Help system??


        // Console???


        // Start HeartBeat
        mHeartBeat = new HeartBeat();
        mHeartBeat.start();

        // "Register"
        getServletContext().setAttribute(BASE_SERVLET_KEY, this);

        //
        LogUtil.getLogger().exiting("BaseServlet","initServlet");
    }

    protected IProcessor initializeProcessor()
    {
        return new BaseProcessor();
    }


    public void destroy()
    {
        if(mHeartBeat != null) {
            mHeartBeat.markToBeStopped();
        }
        getServletContext().removeAttribute(BASE_SERVLET_KEY);
    }

    public void init()
        throws ServletException
    {
        synchronized(this) {
            if(getServletStartTime() <= 0L) {
                setServletStartTime(System.currentTimeMillis());
            }
            // Using the properties file for this...
            //LogUtil.getLogger().setLevel(Level.FINER);

            //
            initServlet();
            //
        }
    }

    public static synchronized void setServletStartTime(long ntime)
    {
        // Why do we truncate the last three digits?????????????????
        // I don't know why... But, without this, "last-modified" does not work for some reason..........
        s_baseServletStarted = (ntime/1000L) * 1000L;
    }

    public static long getServletStartTime()
    {
        return s_baseServletStarted;
    }

    protected long getLastModified(HttpServletRequest req)
    {
        long servletStarted = getServletStartTime();
        long currentTime = System.currentTimeMillis();
        long lastModified = currentTime;  // default

        //

        if(lastModified >= currentTime) {                             // Note the equal sign here....
            lastModified = (System.currentTimeMillis()/1000L)*1000L;  // This will be slightly bigger than currentTime ;-)
        }

        return lastModified;
    }

    public String getServletInfo()
    {
        return "...";
    }


    public void doGet(HttpServletRequest request, HttpServletResponse response)
        throws IOException, ServletException
    {
        process(request, response);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response)
        throws IOException, ServletException
    {
        process(request, response);
    }


    private void process(HttpServletRequest request, HttpServletResponse response)
        throws IOException, ServletException
    {
        LogUtil.getLogger().entering("BaseServlet","process", new Object[]{request,response});

        //
        mAppProcessor.process(request, response);
        //

        LogUtil.getLogger().exiting("BaseServlet","process");
    }

}





