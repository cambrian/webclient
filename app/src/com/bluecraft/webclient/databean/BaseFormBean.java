/*************************************************************************
 *  Copyright (c) 2002-2003, BlueCraft Software. 
 *  All Rights Reserved. No part of this program may be copied
 *  or used without the express written consent of BlueCraft Software.
 *************************************************************************/
// $Id: BaseFormBean.java,v 1.2 2003/02/02 23:12:10 hyoon Exp $ 

package com.bluecraft.webclient.databean;

import com.bluecraft.webclient.*;
import com.bluecraft.webclient.parser.*;
import com.bluecraft.webclient.page.*;

import java.util.HashSet;
import java.lang.reflect.Method;


/**   
 *   
 * @author   <A href="mailto:hyoon@bluecraft.com">Hyoungsoo Yoon</A>
 * @version  $Revision: 1.2 $
 */   
public class BaseFormBean
    implements IFormBean
{
    //
    private static final String SET      = "set";
    private static final String PROCESS  = "process";

    protected HashSet mRequiredFields = new HashSet(5);

    public BaseFormBean()
    {
        super();
        //
        initRequiredFields();

    }


    protected void initRequiredFields()
    {
        mRequiredFields = new HashSet(5);
    }

    public String[] getRequiredFields()
    {
        return (String[])mRequiredFields.toArray();
        //return (String[])mRequiredFields.toArray(mReqFieldsArray);
    }

    public boolean isRequiredField(String aFieldName)
    {
        return mRequiredFields.contains(aFieldName);
    }

    
    protected boolean processAction(String name, IParameterParser values)
        throws IllegalStateException
    {
        String methodName = PROCESS + name;

        try {
            Class[] parameterTypes = {Class.forName("com.bluecraft.webclient.parser.ParameterParser"),
            						  Class.forName("com.bluecraft.webclient.page.PageStatus")
            						 };
            Method proc = this.getClass().getMethod(methodName, parameterTypes);
            Object[] parameters = {values};
            Boolean bSuc = (Boolean) proc.invoke(this, parameters);
            if(bSuc != null) {
                return bSuc.booleanValue();
            } else {
                return false;
            }
        } catch (NoSuchMethodException ne) {
            // We ingore if the action name doesn't exist (for now).
            ne.printStackTrace();
            //throw new IllegalStateException ("No set method exists for action " + name);
        } catch (Exception ex) {
            ex.printStackTrace();
            //throw new IllegalStateException(ex.getMessage());
        }
        return false;
    }

    public PageStatusList submit(IRequest req)
        throws IllegalStateException
    {
        PageStatusList status = new PageStatusList();
        IParameterParser parser = req.getParameterParser();

        String[] actions = null;
        //try {
            actions = parser.getStringValues("key");
            //
        //} catch (ParameterNotFoundException pnfe) {
        //}

        if (actions != null) {
            for (int i = 0; i < actions.length; i++) {
                // If any of the actions fail (either returns false or throws exception),
                // we stop processing the rest.
                if(!processAction(actions[i], parser)) {
                    LogUtil.getLogger().fine("[HY] Only " + i + " actions have been successfully processed out of " + actions.length);
                    break;
                }
            }
        }

        return status;
    }


}



