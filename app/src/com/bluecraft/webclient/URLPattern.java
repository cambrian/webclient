/*************************************************************************
 *  Copyright (c) 2002-2003, BlueCraft Software. 
 *  All Rights Reserved. No part of this program may be copied
 *  or used without the express written consent of BlueCraft Software.
 *************************************************************************/
// $Id: URLPattern.java,v 1.2 2003/02/02 23:12:10 hyoon Exp $ 

package com.bluecraft.webclient;

import javax.servlet.Servlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.StringTokenizer;

    
/**   
 *   
 * @author   <A href="mailto:hyoon@bluecraft.com">Hyoungsoo Yoon</A>
 * @version  $Revision: 1.2 $
 */   
public class URLPattern
    implements Comparable
{
    public static final int PATTERN_TYPE_UNKNOWN = 0;
    public static final int PATTERN_TYPE_DEFAULT_MAPPING = 1;
    public static final int PATTERN_TYPE_EXACT_MAPPING = 2;
    public static final int PATTERN_TYPE_PATH_MAPPING = 4;
    public static final int PATTERN_TYPE_EXTENSION_MAPPING = 8;

    private static final String DEFAULT_MAPPING_PATTERN = "/";
    private static final String WILDCARD = "*";

    public static final URLPattern DEFAULT_PATTERN = new URLPattern(DEFAULT_MAPPING_PATTERN);

    
    private String  mPatternString;
    private int mPatternType;
    private int mPathLength;  // Only to be used for PathMapping
    

    public URLPattern()
    {
        this(DEFAULT_MAPPING_PATTERN);
    }
    
    public URLPattern(String pattern)
    {
        mPatternString = pattern;
        parsePattern();
    }

    public String getPatternString()
    {
        return mPatternString;
    }

    public int getPatternType()
    {
        return mPatternType;
    }

    public boolean isWellFormed()
    {
        return (mPatternType != PATTERN_TYPE_UNKNOWN);
    }

    public boolean isDefaultMapping()
    {
        return (mPatternType == PATTERN_TYPE_DEFAULT_MAPPING);
    }

    public boolean isExactMapping()
    {
        return (mPatternType == PATTERN_TYPE_EXACT_MAPPING);
    }

    public boolean isPathMapping()
    {
        return (mPatternType == PATTERN_TYPE_PATH_MAPPING);
    }

    public boolean isExtensionMapping()
    {
        return (mPatternType == PATTERN_TYPE_EXTENSION_MAPPING);
    }

    public int getPathLength()
    {
        return mPathLength;
    }

    public boolean isMatch(String target)
    {
        if(!isWellFormed()) {
            return false;
        }
        if(target == null) {
            if(isDefaultMapping()) {
                return true;
            } else {
                return false;
            }
        }
        if(isDefaultMapping()) {
            // Default mapping matches anything...
            return true;
        }
        if(isExactMapping()) {
            if(target.equals(mPatternString)) {
                return true;
            } else {
                return false;
            }
        }
        if(isPathMapping()) {
            String pattern = mPatternString.substring(0,mPatternString.length()-2);
            if(target.startsWith(pattern)) {
                return true;
            } else {
                return false;
            }
        }
        if(isExtensionMapping()) {
            String ext = mPatternString.substring(2);
            if(target.endsWith(ext)) {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    public String getTrailingPath(String target)
    {
        if(!isWellFormed()) {
            return null;
        }
        if(!isMatch(target)) {
            return null;
        }

        if(isExactMapping()) {
            return null;
        }
        if(isExtensionMapping()) {
            return null;
        }
        if(isDefaultMapping()) {
            return target;
        }
        if(isPathMapping()) {
            //int idx = mPatternString.indexOf(WILDCARD) - 1;
            int idx = mPatternString.length() - 2;
            String tpath = target.substring(idx);
            return tpath;
        }

        return null;
    }

    public int compareTo(Object o)
    {
        return compareTo((URLPattern) o);
    }

    public int compareTo(URLPattern u)
    {
        // Natural order is Descending!
        if(getPathLength() > u.getPathLength()) {
            return -1;
        } else if(getPathLength() < u.getPathLength()) {
            return 1;
        } else {
            // Just to be consistent, make it descending also.
            int c = mPatternString.compareTo(u.getPatternString());
            return (-1)*c;
        }
    }

    public String toString()
    {
        return "PatternType=" + Integer.toString(mPatternType) + ";PatternString=" + mPatternString;
    }

    private boolean parsePattern()
    {
        if(mPatternString == null) {
            mPatternType = PATTERN_TYPE_UNKNOWN;
            mPathLength = 0;
            return false;
        } else {
            mPatternType = parsePattern(mPatternString);
            if(mPatternType == PATTERN_TYPE_PATH_MAPPING) {
                mPathLength = getPathLength(mPatternString);
                //LogUtil.getLogger().fine("URLPattern.parsePattern(): patternString=" + mPatternString + ";pathLength=" + mPathLength);
            }
            return true;
        }
    }

    private static int getPathLength(String pattern)
    {
        StringTokenizer st = new StringTokenizer(pattern, "/");
        return st.countTokens();
    }
    
    private static int parsePattern(String pattern)
    {
        if(pattern == null) {
            return PATTERN_TYPE_UNKNOWN;
        }

        if(pattern.equals(DEFAULT_MAPPING_PATTERN)) {
            return PATTERN_TYPE_DEFAULT_MAPPING;
        }
        if(pattern.indexOf(WILDCARD) == -1) {
            if(pattern.startsWith("/")) {
                return PATTERN_TYPE_EXACT_MAPPING;
            } else {
                return PATTERN_TYPE_UNKNOWN;
            }
        }
        if(pattern.endsWith(WILDCARD)) {
            if(pattern.indexOf(WILDCARD) == pattern.lastIndexOf(WILDCARD)
               && pattern.startsWith("/")
               && pattern.length() > 1
               && pattern.lastIndexOf("/") == pattern.length()-2) {
                return PATTERN_TYPE_PATH_MAPPING;
            } else {
                return PATTERN_TYPE_UNKNOWN;
            }
        }
        if(pattern.startsWith(WILDCARD)) {
            if(pattern.indexOf(WILDCARD) == pattern.lastIndexOf(WILDCARD)) {
                if(pattern.length() > 2
                   && pattern.indexOf(".") == 1
                   && pattern.indexOf("/") == -1) {
                    return PATTERN_TYPE_EXTENSION_MAPPING;
                } else {
                    return PATTERN_TYPE_UNKNOWN;
                }
            } else {
                return PATTERN_TYPE_UNKNOWN;
            }
        }

        return PATTERN_TYPE_UNKNOWN;
    }
    
}



