/*************************************************************************
 *  Copyright (c) 2002-2003, BlueCraft Software. 
 *  All Rights Reserved. No part of this program may be copied
 *  or used without the express written consent of BlueCraft Software.
 *************************************************************************/
// $Id: StatusInfo.java,v 1.2 2003/02/02 23:12:11 hyoon Exp $ 

package com.bluecraft.webclient.status;

import com.bluecraft.webclient.*;

import javax.servlet.http.*;


/**   
 *   
 * @author   <A href="mailto:hyoon@bluecraft.com">Hyoungsoo Yoon</A>
 * @version  $Revision: 1.2 $
 */   
public class StatusInfo
    implements IStatus
{
    int     mCode;
    String  mMessage;
    
    public StatusInfo(int code, String msg)
    {
        mCode = code;
        mMessage = msg;
    }
    
    public int      getCode()
    {
        return mCode;
    }

    public String   getMessage()
    {
        return mMessage;
    }


    public boolean  is100()
    {
        if(mCode >= 100 && mCode < 200) {
            return true;
        } else {
            return false;
        }
    }

    public boolean  is200()
    {
        if(mCode >= 200 && mCode < 300) {
            return true;
        } else {
            return false;
        }
    }

    public boolean  is300()
    {
        if(mCode >= 300 && mCode < 400) {
            return true;
        } else {
            return false;
        }
    }

    public boolean  is400()
    {
        if(mCode >= 400 && mCode < 500) {
            return true;
        } else {
            return false;
        }
    }

    public boolean  is500()
    {
        if(mCode >= 500 && mCode < 600) {
            return true;
        } else {
            return false;
        }
    }

}



