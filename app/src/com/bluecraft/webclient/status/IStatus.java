/*************************************************************************
 *  Copyright (c) 2002-2003, BlueCraft Software. 
 *  All Rights Reserved. No part of this program may be copied
 *  or used without the express written consent of BlueCraft Software.
 *************************************************************************/
// $Id: IStatus.java,v 1.2 2003/02/02 23:12:11 hyoon Exp $ 

package com.bluecraft.webclient.status;

import com.bluecraft.webclient.*;


/**   
 *   
 * @author   <A href="mailto:hyoon@bluecraft.com">Hyoungsoo Yoon</A>
 * @version  $Revision: 1.2 $
 */   
public interface IStatus
{
    int      getCode();
    String   getMessage();

    boolean  is100();
    boolean  is200();
    boolean  is300();
    boolean  is400();
    boolean  is500();
}



