/*************************************************************************
 *  Copyright (c) 2002-2003, BlueCraft Software. 
 *  All Rights Reserved. No part of this program may be copied
 *  or used without the express written consent of BlueCraft Software.
 *************************************************************************/
// $Id: DefaultApplicationManager.java,v 1.2 2003/02/02 23:12:10 hyoon Exp $

package com.bluecraft.webclient;

import com.bluecraft.webclient.cache.*;
import com.bluecraft.webclient.config.*;
import com.bluecraft.webclient.resource.*;
import com.bluecraft.webclient.util.*;
import com.bluecraft.common.Guid;
import com.bluecraft.version.BaseVersionable;

import javax.servlet.Servlet;
import javax.servlet.ServletContext;
import java.util.logging.Logger;
import java.util.Locale;
import java.util.Map;
import java.util.HashMap;
import java.util.Set;
import java.util.Iterator;


/**
 *
 * @author   <A href="mailto:hyoon@bluecraft.com">Hyoungsoo Yoon</A>
 * @version  $Revision: 1.2 $
 */
public class DefaultApplicationManager
    extends BaseVersionable
    implements IApplicationManager
{
    // Cache ServletContext
    private ServletContext mServletContext = null;

    private IConfigManager mConfigManager = null;

    private IResourceManager mResourceManager = null;


    /**
     * List of the servlets registered in our web application deployment descriptor
     * and loaded into the container.
     */
    private Map    mServletMap = null;


    protected DefaultApplicationManager()
    {
        // ???
    }

    public DefaultApplicationManager(ServletContext servletContext)
    {
        mServletContext = servletContext;
        mServletMap = new HashMap();

        initializeConfigManager();
        initializeResourceManager();
    }

    private void initializeConfigManager()
    {
        LogUtil.getLogger().entering("DefaultApplicationManager","initializeConfigManager");

        mConfigManager = new DefaultConfigManager(mServletContext);
        //mConfigManager.addConfig();


        LogUtil.getLogger().exiting("DefaultApplicationManager","initializeConfigManager");
    }

    private void initializeResourceManager()
    {
        LogUtil.getLogger().entering("DefaultApplicationManager","initializeResourceManager");

        mResourceManager = new DefaultResourceManager(mServletContext);


        LogUtil.getLogger().exiting("DefaultApplicationManager","initializeResourceManager");
    }


    //public String getContextPath();

    public String getContextName()
    {
        return mServletContext.getServletContextName();
    }

    public ServletContext getContext()
    {
        return mServletContext;
    }


    public IConfigManager   getConfigManager()
    {
        return mConfigManager;
    }

    public IResourceManager getResourceManager()
    {
        return mResourceManager;
    }


    public void setServlet(Guid servletID, IServlet servlet)
    {
        // Can we keep the reference to the servlet at all????
        mServletMap.put(servletID, servlet);
    }

    public Guid registerServlet(IServlet servlet)
    {
        // Can we keep the reference to the servlet at all????
        Guid servletID = new Guid();
        mServletMap.put(servletID, servlet);
        return servletID;
    }


    //String getServletPath(String servletName);
    //String getServletPath(Guid servletID);


    public IServlet getServlet(Guid servletID)
    {
        // What happens if the servlet has been reloaded????
        return (IServlet) mServletMap.get(servletID);
    }

    public IServlet[] getServlets()
    {
        // TODO:
        return new IServlet[] {};
    }

    public IServlet getServlet(String servletName)
    {
        IServlet[] servlets = getServlets(servletName);
        if(servlets != null && servlets.length > 0) {
            return servlets[0];
        } else {
            return null;
        }
    }

    public IServlet[] getServlets(String servletName)
    {
        // TODO:
        return new IServlet[] {};
    }

    public String getServletName(Guid servletID)
    {
        // TODO:
        return null;
    }

    public String[] getServletNames()
    {
        // TODO:
        return new String[] {};
    }



    public boolean equals(Object o)
    {
        if(o == null || !(o instanceof DefaultApplicationManager)) {
            return false;
        }
        return equals((DefaultApplicationManager) o);
    }

    public boolean equals(DefaultApplicationManager appManager)
    {
        if(appManager == null) {
            return false;
        }
        return mServletContext.equals(appManager.getContext());
    }


    //Somethig's wrong with this?????
    public int hashCode()
    {
        if(mServletContext != null) {
            return mServletContext.hashCode();
        } else {
            return super.hashCode();
        }
    }

    /* */
    public String toString()
    {
        return "ApplicationManager = " + mServletContext;
    }
    /* */

}




