/*************************************************************************
 *  Copyright (c) 2002-2003, BlueCraft Software. 
 *  All Rights Reserved. No part of this program may be copied
 *  or used without the express written consent of BlueCraft Software.
 *************************************************************************/
// $Id: IApplicationManager.java,v 1.2 2003/02/02 23:12:10 hyoon Exp $ 

package com.bluecraft.webclient;

import com.bluecraft.webclient.config.*;
import com.bluecraft.webclient.resource.*;
import com.bluecraft.webclient.util.*;
import com.bluecraft.common.Guid;
import com.bluecraft.version.IVersionable;

import javax.servlet.Servlet;
import javax.servlet.ServletContext;


/**   
 *   
 * @author   <A href="mailto:hyoon@bluecraft.com">Hyoungsoo Yoon</A>
 * @version  $Revision: 1.2 $
 */   
public interface IApplicationManager
    extends IVersionable
{
    //String getContextPath();
    String getContextName();
    ServletContext getContext();

    IConfigManager   getConfigManager();
    IResourceManager getResourceManager();
    
    void setServlet(Guid servletID, IServlet servlet);
    Guid registerServlet(IServlet servlet);
  
    //String getServletPath(String servletName);
    String     getServletName(Guid servletID);
    String[]   getServletNames();
    IServlet   getServlet(Guid servletID);
    IServlet[] getServlets();
    IServlet[] getServlets(String servletName);
    IServlet   getServlet(String servletName);

}





