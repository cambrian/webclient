/*************************************************************************
 *  Copyright (c) 2002-2003, BlueCraft Software. 
 *  All Rights Reserved. No part of this program may be copied
 *  or used without the express written consent of BlueCraft Software.
 *************************************************************************/
// $Id: IFormBeanMappingList.java,v 1.2 2003/02/02 23:12:10 hyoon Exp $ 

package com.bluecraft.webclient.mapping;

import com.bluecraft.webclient.*;
import com.bluecraft.webclient.page.*;
import com.bluecraft.webclient.databean.*;


/**   
 *   
 * @author   <A href="mailto:hyoon@bluecraft.com">Hyoungsoo Yoon</A>
 * @version  $Revision: 1.2 $
 */   
public interface IFormBeanMappingList
    extends IMappingList
{
    boolean            addFormBeanMapping(FormBeanMapping formBeanMapping);
    boolean            addFormBeanMapping(IForm form, IFormBean formBean);
    IFormBean          getFormBean(IForm form);
    //FormBeanMapping  getFormBeanMapping(IForm form);

    //void             clearList();
    //IFormBean        removeFormBeanMapping(IForm form);
    boolean            containsFormBeanMapping(IForm form);
}


