/*************************************************************************
 *  Copyright (c) 2002-2003, BlueCraft Software. 
 *  All Rights Reserved. No part of this program may be copied
 *  or used without the express written consent of BlueCraft Software.
 *************************************************************************/
// $Id: DefaultFormBeanMappingList.java,v 1.2 2003/02/02 23:12:10 hyoon Exp $ 

package com.bluecraft.webclient.mapping;

import com.bluecraft.webclient.*;
import com.bluecraft.webclient.page.*;
import com.bluecraft.webclient.databean.*;

import java.util.Map;
import java.util.HashMap;
import java.util.Set;
import java.util.Iterator;


/**   
 *   
 * @author   <A href="mailto:hyoon@bluecraft.com">Hyoungsoo Yoon</A>
 * @version  $Revision: 1.2 $
 */   
public class DefaultFormBeanMappingList
    implements IFormBeanMappingList
{
    private Map   mMap = null;
    
    public DefaultFormBeanMappingList()
    {
        mMap = new HashMap();
        
        // TODO: initialize the list from a property file???
        
    }
    public boolean        containsFormBeanMapping(IForm form)
    {
        if(mMap.containsKey(form)) {
            return true;
        } else {
            return false;
        }
    }
    
    public boolean        addFormBeanMapping(FormBeanMapping formBeanMapping)
    {
        IForm     form = formBeanMapping.getForm();
        IFormBean    formBean = formBeanMapping.getFormBean();
        if(form == null) {
            //LogUtil.getLogger().fine("addFormBeanMapping() Failed: form is null.");
            return false;
        }
        if(containsFormBeanMapping(form)) {
            //LogUtil.getLogger().fine("addFormBeanMapping() Failed: form is already in the map.");
            return false;
        }
        mMap.put(form, formBean);
        return true;
    }

    public boolean        addFormBeanMapping(IForm form, IFormBean formBean)
    {
        return addFormBeanMapping(new FormBeanMapping(form, formBean));
    }

    public IFormBean        getFormBean(IForm form)
    {
        if(mMap.containsKey(form)) {
            return (IFormBean) mMap.get(form);
        } else {
            return null;
        }
    }
    
}


