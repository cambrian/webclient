/*************************************************************************
 *  Copyright (c) 2002-2003, BlueCraft Software. 
 *  All Rights Reserved. No part of this program may be copied
 *  or used without the express written consent of BlueCraft Software.
 *************************************************************************/
// $Id: DefaultActionMappingList.java,v 1.2 2003/02/02 23:12:10 hyoon Exp $ 

package com.bluecraft.webclient.mapping;

import com.bluecraft.webclient.*;
import com.bluecraft.webclient.action.*;

import java.util.Map;
import java.util.SortedMap;
import java.util.HashMap;
import java.util.TreeMap;
import java.util.Set;
import java.util.Iterator;


/**   
 *   
 * @author   <A href="mailto:hyoon@bluecraft.com">Hyoungsoo Yoon</A>
 * @version  $Revision: 1.2 $
 */   
public class DefaultActionMappingList
    implements IActionMappingList
{
    private Map   mMapDefaultMapping = null; // Well, only 0 or 1 element in this map.
    private Map   mMapExactMapping = null;
    private SortedMap   mMapPathMapping = null;
    private Map   mMapExtensionMapping = null;
    
    public DefaultActionMappingList()
    {
        mMapDefaultMapping = new HashMap();
        mMapExactMapping = new HashMap();
        mMapPathMapping = new TreeMap();
        mMapExtensionMapping = new HashMap();
        
        // TODO: initialize the list from a property file???
        
    }
    public boolean        containsActionMapping(URLPattern pattern)
    {
        if(mMapDefaultMapping.containsKey(pattern) ||
           mMapExactMapping.containsKey(pattern) ||
           mMapPathMapping.containsKey(pattern) ||
           mMapExtensionMapping.containsKey(pattern)) {
            return true;
        } else {
            return false;
        }
    }
    
    public boolean        addActionMapping(ActionMapping actionMapping)
    {
        URLPattern pattern = actionMapping.getPattern();
        IAction    action = actionMapping.getAction();
        if(!pattern.isWellFormed()) {
            //LogUtil.getLogger().fine("addActionMapping() Failed: pattern is not well formed.");
            return false;
        }
        if(containsActionMapping(pattern)) {
            //LogUtil.getLogger().fine("addActionMapping() Failed: pattern is already in the map.");
            return false;
        }
        if(pattern.isDefaultMapping()) {
            mMapDefaultMapping.put(pattern, action);
            return true;
        } else if(pattern.isExactMapping()) {
            mMapExactMapping.put(pattern, action);
            return true;
        } else if(pattern.isPathMapping()) {
            mMapPathMapping.put(pattern, action);
            return true;
        } else if(pattern.isExtensionMapping()) {
            mMapExtensionMapping.put(pattern, action);
            return true;
        }
        return false;
    }

    public boolean        addActionMapping(URLPattern pattern, IAction action)
    {
        return addActionMapping(new ActionMapping(pattern, action));
    }

    public IAction        getAction(URLPattern pattern)
    {
        if(mMapDefaultMapping.containsKey(pattern)) {
            return (IAction) mMapDefaultMapping.get(pattern);
        } else if(mMapExactMapping.containsKey(pattern)) {
            return (IAction) mMapExactMapping.get(pattern);
        } else if(mMapPathMapping.containsKey(pattern)) {
            return (IAction) mMapPathMapping.get(pattern);
        } else if(mMapExtensionMapping.containsKey(pattern)) {
            return (IAction) mMapExtensionMapping.get(pattern);
        } else {
            return null;
        }
    }
    
    public IAction        getAction(String urlString)
    {
        URLPattern urlPat = new URLPattern(urlString);
        if(!urlPat.isExactMapping()) {
            return null;
        }

        IAction action = null;
        action = (IAction) mMapExactMapping.get(urlPat);
        if(action != null) {
            return action;
        }

        Set pset = mMapPathMapping.keySet();
        //LogUtil.getLogger().fine("PathMap size = " + mMapPathMapping.size());
        //LogUtil.getLogger().fine("KeySet size = " + pset.size());

        Iterator pit = pset.iterator();
        while(pit.hasNext()) {
            URLPattern pattern = (URLPattern) pit.next();
            //LogUtil.getLogger().fine("pattern = " + pattern);
            if(pattern.isMatch(urlString)) {
                action = (IAction) mMapPathMapping.get(pattern);
                if(action != null) {
                    return action;
                }
            }
        }

        Set eset = mMapExtensionMapping.keySet();
        Iterator eit = eset.iterator();
        while(eit.hasNext()) {
            URLPattern pattern = (URLPattern) eit.next();
            if(pattern.isMatch(urlString)) {
                action = (IAction) mMapExtensionMapping.get(pattern);
                if(action != null) {
                    return action;
                }
            }
        }

        if(mMapDefaultMapping.size() == 1) {
            action = (IAction) mMapDefaultMapping.get(URLPattern.DEFAULT_PATTERN);
            if(action != null) { // redundant.
                return action;
            }
        }
        
        return action;
    }

}



