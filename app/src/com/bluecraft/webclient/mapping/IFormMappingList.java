/*************************************************************************
 *  Copyright (c) 2002-2003, BlueCraft Software. 
 *  All Rights Reserved. No part of this program may be copied
 *  or used without the express written consent of BlueCraft Software.
 *************************************************************************/
// $Id: IFormMappingList.java,v 1.2 2003/02/02 23:12:10 hyoon Exp $ 

package com.bluecraft.webclient.mapping;

import com.bluecraft.webclient.*;
import com.bluecraft.webclient.page.*;

import com.bluecraft.common.Guid;


/**   
 *   
 * @author   <A href="mailto:hyoon@bluecraft.com">Hyoungsoo Yoon</A>
 * @version  $Revision: 1.2 $
 */   
public interface IFormMappingList
    extends IMappingList
{
    boolean        addFormMapping(FormMapping formMapping);
    boolean        addFormMapping(Guid id, IForm form);
    IForm          getForm(Guid id);
    //FormMapping  getFormMapping(Guid id);

    //void         clearList();
    //IForm        removeFormMapping(Guid id);
    boolean        containsFormMapping(Guid id);
}



