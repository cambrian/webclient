/*************************************************************************
 *  Copyright (c) 2002-2003, BlueCraft Software. 
 *  All Rights Reserved. No part of this program may be copied
 *  or used without the express written consent of BlueCraft Software.
 *************************************************************************/
// $Id: DefaultFormMappingList.java,v 1.2 2003/02/02 23:12:10 hyoon Exp $ 

package com.bluecraft.webclient.mapping;

import com.bluecraft.webclient.*;
import com.bluecraft.webclient.page.*;

import com.bluecraft.common.Guid;

import java.util.Map;
import java.util.HashMap;
import java.util.Set;
import java.util.Iterator;


/**   
 *   
 * @author   <A href="mailto:hyoon@bluecraft.com">Hyoungsoo Yoon</A>
 * @version  $Revision: 1.2 $
 */   
public class DefaultFormMappingList
    implements IFormMappingList
{
    private Map   mMap = null;
    
    public DefaultFormMappingList()
    {
        mMap = new HashMap();
        
        // TODO: initialize the list from a property file???
        
    }
    public boolean        containsFormMapping(Guid id)
    {
        if(mMap.containsKey(id)) {
            return true;
        } else {
            return false;
        }
    }
    
    public boolean        addFormMapping(FormMapping formMapping)
    {
        Guid     id = formMapping.getIdentifier();
        IForm    form = formMapping.getForm();
        if(id == null || id.equals(Guid.NULL_GUID)) {
            //LogUtil.getLogger().fine("addFormMapping() Failed: id is null.");
            return false;
        }
        if(containsFormMapping(id)) {
            //LogUtil.getLogger().fine("addFormMapping() Failed: id is already in the map.");
            return false;
        }
        mMap.put(id, form);
        return true;
    }

    public boolean        addFormMapping(Guid id, IForm form)
    {
        return addFormMapping(new FormMapping(id, form));
    }

    public IForm          getForm(Guid id)
    {
        if(mMap.containsKey(id)) {
            return (IForm) mMap.get(id);
        } else {
            return null;
        }
    }
    
}


