/*************************************************************************
 *  Copyright (c) 2002-2003, BlueCraft Software. 
 *  All Rights Reserved. No part of this program may be copied
 *  or used without the express written consent of BlueCraft Software.
 *************************************************************************/
// $Id: IActionMappingList.java,v 1.2 2003/02/02 23:12:10 hyoon Exp $ 

package com.bluecraft.webclient.mapping;

import com.bluecraft.webclient.*;
import com.bluecraft.webclient.action.*;


/**   
 *   
 * @author   <A href="mailto:hyoon@bluecraft.com">Hyoungsoo Yoon</A>
 * @version  $Revision: 1.2 $
 */   
public interface IActionMappingList
    extends IMappingList
{
    boolean        addActionMapping(ActionMapping actionMapping);
    boolean        addActionMapping(URLPattern pattern, IAction action);
    IAction        getAction(URLPattern pattern);
    IAction        getAction(String urlString);
    //ActionMapping  getActionMapping(String urlString);
    //ActionMapping  getActionMapping(URLPattern pattern);

    //void           clearList();
    //IAction        removeActionMapping(URLPattern pattern);
    boolean        containsActionMapping(URLPattern pattern);
}



