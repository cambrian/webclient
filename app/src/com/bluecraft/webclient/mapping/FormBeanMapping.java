/*************************************************************************
 *  Copyright (c) 2002-2003, BlueCraft Software. 
 *  All Rights Reserved. No part of this program may be copied
 *  or used without the express written consent of BlueCraft Software.
 *************************************************************************/
// $Id: FormBeanMapping.java,v 1.2 2003/02/02 23:12:10 hyoon Exp $ 

package com.bluecraft.webclient.mapping;

import com.bluecraft.webclient.*;
import com.bluecraft.webclient.page.*;
import com.bluecraft.webclient.databean.*;

import javax.servlet.Servlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**   
 *   
 * @author   <A href="mailto:hyoon@bluecraft.com">Hyoungsoo Yoon</A>
 * @version  $Revision: 1.2 $
 */   
public class FormBeanMapping
    implements IMapping
{
    private IForm mForm;
    private IFormBean  mFormBean;

    public FormBeanMapping(IForm form, IFormBean formBean)
    {
        mForm = form;
        mFormBean = formBean;
    }
    
    public IForm getForm()
    {
        return mForm;
    }
    
    public IFormBean getFormBean()
    {
        return mFormBean;
    }
    
}



