/*************************************************************************
 *  Copyright (c) 2002-2003, BlueCraft Software. 
 *  All Rights Reserved. No part of this program may be copied
 *  or used without the express written consent of BlueCraft Software.
 *************************************************************************/
// $Id: BaseRequest.java,v 1.2 2003/02/02 23:12:10 hyoon Exp $ 

package com.bluecraft.webclient;

import com.bluecraft.webclient.parser.*;
import com.bluecraft.webclient.page.*;
import com.bluecraft.webclient.logging.*;

import java.io.InputStream;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.MissingResourceException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.UnavailableException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;
import java.util.logging.Logger;
import java.util.logging.Level;


/**   
 *   
 * @author   <A href="mailto:hyoon@bluecraft.com">Hyoungsoo Yoon</A>
 * @version  $Revision: 1.2 $
 */   
public class BaseRequest
    extends HttpServletRequestWrapper
    implements IRequest
{
    private IHeaderParser     mHeaderParser = null;
    private ICookieParser     mCookieParser = null;
    private IParameterParser  mParamParser = null;
    private IForm             mForm = null;

    public BaseRequest(HttpServletRequest request)
    {
        super(request);
        processMultipart();
    }

    public IForm getForm()
    {
        return mForm;
    }

    public void setForm(IForm form)
    {
        mForm = form;
    }

    public IHeaderParser getHeaderParser()
    {
        return mHeaderParser;
    }

    // ???
    public void setHeaderParser(IHeaderParser headerParser)
    {
        mHeaderParser = headerParser;
    }

    public ICookieParser getCookieParser()
    {
        return mCookieParser;
    }

    // ???
    public void setCookieParser(ICookieParser cookieParser)
    {
        mCookieParser = cookieParser;
    }

    public IParameterParser getParameterParser()
    {
        return mParamParser;
    }

    // ???
    public void setParameterParser(IParameterParser paramParser)
    {
        mParamParser = paramParser;
    }


    protected void processMultipart()
    {
        if (!"POST".equals(getMethod())) {
            return;
        }
        String contentType = getContentType();
        if ((contentType != null) &&
            contentType.startsWith("multipart/form-data")) {
            //
            return;
        } else {
            return;
        }
    }


}



