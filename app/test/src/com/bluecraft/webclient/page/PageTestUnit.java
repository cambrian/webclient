/************************************************************ 
 *  Copyright 2002, BlueCraft Software. All Rights Reserved.      
 *  This software is distributed on an "AS IS" basis,         
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.  
 ************************************************************/
// $Id: PageTestUnit.java,v 1.1 2003/01/05 10:08:02 hyoon Exp $

package com.bluecraft.webclient.page;

import com.bluecraft.webclient.*;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import java.io.*;


/**
 * A TestCase which returns TestSuite.
 * This class tests all TestCases in the webclient page package.
 *
 * @author Hyoungsoo Yoon
 */
public class PageTestUnit
    extends TestCase
{
    /**
     * Constructor.
     */
    public PageTestUnit(String s)
    {
        super(s);
    }

    /**
     * Returns TestSuite.
     */
    public static Test suite()
    {
        TestSuite suite = new TestSuite("WebClient Page Test-Unit");
        suite.addTestSuite(BaseFormTestCase.class);
        suite.addTestSuite(PageStatusTestCase.class);
        return suite;
    }

    /**
     * Invokes the TestRunner.
     */
    public static void main(String args[])
    {
        junit.textui.TestRunner.run(suite());
        //junit.swingui.TestRunner.run(PageTestUnit.class);
        //junit.swingui.TestRunner.run(new String[] {PageTestUnit.class.getName()});
    }

}


