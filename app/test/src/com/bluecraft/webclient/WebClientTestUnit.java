/************************************************************ 
 *  Copyright 2002, BlueCraft Software. All Rights Reserved.      
 *  This software is distributed on an "AS IS" basis,         
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.  
 ************************************************************/
// $Id: WebClientTestUnit.java,v 1.1 2003/01/05 10:08:02 hyoon Exp $

package com.bluecraft.webclient;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import java.io.*;


/**
 * A TestCase which returns TestSuite.
 * This class tests all TestCases in the webclient package.
 *
 * @author Hyoungsoo Yoon
 */
public class WebClientTestUnit
    extends TestCase
{
    /**
     * Constructor.
     */
    public WebClientTestUnit(String s)
    {
        super(s);
    }

    /**
     * Returns TestSuite.
     */
    public static Test suite()
    {
        TestSuite suite = new TestSuite("WebClient Test-Unit");
        suite.addTestSuite(AnchorTestCase.class);
        suite.addTestSuite(URLPatternTestCase.class);
        suite.addTestSuite(BaseRequestTestCase.class);
        suite.addTestSuite(BaseResponseTestCase.class);
        suite.addTestSuite(BaseSessionTestCase.class);
        suite.addTestSuite(DefaultHostManagerTestCase.class);
        suite.addTestSuite(DefaultApplicationManagerTestCase.class);
        suite.addTestSuite(BaseServletTestCase.class);
        return suite;
    }

    /**
     * Invokes the TestRunner.
     */
    public static void main(String args[])
    {
        junit.textui.TestRunner.run(suite());
        //junit.swingui.TestRunner.run(WebClientTestUnit.class);
        //junit.swingui.TestRunner.run(new String[] {WebClientTestUnit.class.getName()});
    }

}


