/************************************************************ 
 *  Copyright 2002, BlueCraft Software. All Rights Reserved.      
 *  This software is distributed on an "AS IS" basis,         
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.  
 ************************************************************/
// $Id: ProcessorTestUnit.java,v 1.1 2003/01/05 10:08:02 hyoon Exp $

package com.bluecraft.webclient.processor;

import com.bluecraft.webclient.*;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import java.io.*;


/**
 * A TestCase which returns TestSuite.
 * This class tests all TestCases in the webclient processor package.
 *
 * @author Hyoungsoo Yoon
 */
public class ProcessorTestUnit
    extends TestCase
{
    /**
     * Constructor.
     */
    public ProcessorTestUnit(String s)
    {
        super(s);
    }

    /**
     * Returns TestSuite.
     */
    public static Test suite()
    {
        TestSuite suite = new TestSuite("WebClient Processor Test-Unit");
        suite.addTestSuite(BaseProcessorTestCase.class);
        return suite;
    }

    /**
     * Invokes the TestRunner.
     */
    public static void main(String args[])
    {
        junit.textui.TestRunner.run(suite());
        //junit.swingui.TestRunner.run(ProcessorTestUnit.class);
        //junit.swingui.TestRunner.run(new String[] {ProcessorTestUnit.class.getName()});
    }

}


