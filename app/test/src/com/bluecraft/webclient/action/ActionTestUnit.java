/************************************************************ 
 *  Copyright 2002, BlueCraft Software. All Rights Reserved.      
 *  This software is distributed on an "AS IS" basis,         
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.  
 ************************************************************/
// $Id: ActionTestUnit.java,v 1.1 2003/01/05 10:08:02 hyoon Exp $

package com.bluecraft.webclient.action;

import com.bluecraft.webclient.*;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import java.io.*;


/**
 * A TestCase which returns TestSuite.
 * This class tests all TestCases in the webclient action package.
 *
 * @author Hyoungsoo Yoon
 */
public class ActionTestUnit
    extends TestCase
{
    /**
     * Constructor.
     */
    public ActionTestUnit(String s)
    {
        super(s);
    }

    /**
     * Returns TestSuite.
     */
    public static Test suite()
    {
        TestSuite suite = new TestSuite("WebClient Action Test-Unit");
        suite.addTestSuite(BaseActionTestCase.class);
        suite.addTestSuite(BaseActionRouterTestCase.class);
        suite.addTestSuite(DefaultActionManagerTestCase.class);
        return suite;
    }

    /**
     * Invokes the TestRunner.
     */
    public static void main(String args[])
    {
        junit.textui.TestRunner.run(suite());
        //junit.swingui.TestRunner.run(ActionTestUnit.class);
        //junit.swingui.TestRunner.run(new String[] {ActionTestUnit.class.getName()});
    }

}


