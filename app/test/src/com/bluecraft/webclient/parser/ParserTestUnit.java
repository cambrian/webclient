/************************************************************ 
 *  Copyright 2002, BlueCraft Software. All Rights Reserved.      
 *  This software is distributed on an "AS IS" basis,         
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.  
 ************************************************************/
// $Id: ParserTestUnit.java,v 1.1 2003/01/05 10:08:02 hyoon Exp $

package com.bluecraft.webclient.parser;

import com.bluecraft.webclient.*;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import java.io.*;


/**
 * A TestCase which returns TestSuite.
 * This class tests all TestCases in the webclient parser package.
 *
 * @author Hyoungsoo Yoon
 */
public class ParserTestUnit
    extends TestCase
{
    /**
     * Constructor.
     */
    public ParserTestUnit(String s)
    {
        super(s);
    }

    /**
     * Returns TestSuite.
     */
    public static Test suite()
    {
        TestSuite suite = new TestSuite("WebClient Parser Test-Unit");
        suite.addTestSuite(HeaderParserTestCase.class);
        suite.addTestSuite(CookieParserTestCase.class);
        suite.addTestSuite(ParameterParserTestCase.class);
        return suite;
    }

    /**
     * Invokes the TestRunner.
     */
    public static void main(String args[])
    {
        junit.textui.TestRunner.run(suite());
        //junit.swingui.TestRunner.run(ParserTestUnit.class);
        //junit.swingui.TestRunner.run(new String[] {ParserTestUnit.class.getName()});
    }

}


