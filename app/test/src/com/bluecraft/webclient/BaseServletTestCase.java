/************************************************************ 
 *  Copyright 2002, BlueCraft Software. All Rights Reserved.      
 *  This software is distributed on an "AS IS" basis,         
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.  
 ************************************************************/
// $Id: BaseServletTestCase.java,v 1.1 2003/01/05 10:08:02 hyoon Exp $

package com.bluecraft.webclient;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import org.apache.cactus.Cookie;
import org.apache.cactus.ServletTestCase;
import org.apache.cactus.WebRequest;
import org.apache.cactus.WebResponse;

import java.io.*;


/**
 * A TestCase for BaseServlet.
 *
 * @author Hyoungsoo Yoon
 */
public class BaseServletTestCase
    extends ServletTestCase
{
    /**
     * Constructor.
     */
    public BaseServletTestCase(String s)
    {
        super(s);
    }

    /**
     * Sets up the fixture, for example, open a network connection.
     * This method is called before a test is executed.
     */
    protected void setUp()
        throws Exception
    {
        super.setUp();
        //
    }

    /**
     * Tears down the fixture, for example, close a network connection.
     * This method is called after a test is executed
     */
    protected void tearDown()
        throws Exception
    {
        super.tearDown();
        //
    }


    /**
     * A dummy test function which is supposed succeed.
     */
    public void testDummy1()
    {
        Boolean b_expected = new Boolean(true);
        Boolean b_actual = new Boolean(true);
        assertEquals("Comparing two Booleans", b_expected, b_actual);
    }

    /**
     * A dummy test function which is supposed fail.
     */
    public void testDummy2()
    {
        Boolean b_expected = new Boolean(true);
        Boolean b_actual = new Boolean(false);
        assertEquals("Comparing two Booleans", b_expected, b_actual);
    }


}

