/************************************************************ 
 *  Copyright 2002, BlueCraft Software. All Rights Reserved.      
 *  This software is distributed on an "AS IS" basis,         
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.  
 ************************************************************/
// $Id: WebClientTestSuite.java,v 1.1 2003/01/05 10:08:02 hyoon Exp $

package com.bluecraft;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;


/**
 * A TestCase which returns TestSuite.
 * This class tests all TestUnits in the WebClient project.
 *
 * @author Hyoungsoo Yoon
 */
public class WebClientTestSuite
    extends TestCase
{
    /**
     * Constructor.
     */
    public WebClientTestSuite(String s)
    {
        super(s);
    }

    /**
     * Returns TestSuite.
     */
    public static Test suite()
    {
        TestSuite suite = new TestSuite("WebClient Test-Suite");
        suite.addTest(com.bluecraft.webclient.WebClientTestUnit.suite());
        suite.addTest(com.bluecraft.webclient.filter.FilterTestUnit.suite());
        suite.addTest(com.bluecraft.webclient.config.ConfigTestUnit.suite());
        suite.addTest(com.bluecraft.webclient.action.ActionTestUnit.suite());
        suite.addTest(com.bluecraft.webclient.parser.ParserTestUnit.suite());
        suite.addTest(com.bluecraft.webclient.processor.ProcessorTestUnit.suite());
        suite.addTest(com.bluecraft.webclient.page.PageTestUnit.suite());
        return suite;
    }

    /**
     * Invokes the TestRunner.
     */
    public static void main(String args[])
    {
        junit.textui.TestRunner.run(suite());
        //junit.swingui.TestRunner.run(WebClientTestSuite.class);
        //junit.swingui.TestRunner.run(new String[] {WebClientTestSuite.class.getName()});
    }

}


