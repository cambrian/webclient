<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%@ page buffer="16kb" autoFlush="true" %>
<%@ page isErrorPage="true" %>

<HTML lang="en">
<HEAD>
<META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<TITLE>WEB Client | Error Page</TITLE>
<LINK rel="STYLESHEET" href="<%=request.getContextPath()%>/styles/boilerplate.css" type="text/css">
</HEAD>
<BODY class="error" leftMargin="0" topMargin="0" marginHeight="0" marginWidth="0">


Exception <I><%= exception %></I> occurred.

<BR/>
<PRE>
<% exception.printStackTrace(new java.io.PrintWriter(out)); %>
</PRE>


</BODY>
</HTML>

<!--
  Copyright (c) 2002, BlueCraft Software.  All rights reserved.
  Requested URI: <%= request.getRequestURI() %>
  $Id: error.jsp,v 1.2 2003/04/12 20:57:16 hyoon Exp $
-->

