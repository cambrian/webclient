/*************************************************************************
 *  Copyright (c) 2002-2003, BlueCraft Software. 
 *  All Rights Reserved. No part of this program may be copied
 *  or used without the express written consent of BlueCraft Software.
 *************************************************************************/
// $Id: BoilerplateConfigKeys.java,v 1.2 2003/04/12 20:57:16 hyoon Exp $

package com.bluecraft.webclient.boilerplate.config;

import com.bluecraft.webclient.*;
import com.bluecraft.webclient.config.*;


/**   
 *   
 * @author   <A href="mailto:hyoon@bluecraft.com">Hyoungsoo Yoon</A>
 * @version  $Revision: 1.2 $
 */   
public interface BoilerplateConfigKeys
    extends BaseConfigKeys
{
    //static final String PREFERRED_CHANNEL = "PreferredChannel";
    

}



