/*************************************************************************
 *  Copyright (c) 2002-2003, BlueCraft Software. 
 *  All Rights Reserved. No part of this program may be copied
 *  or used without the express written consent of BlueCraft Software.
 *************************************************************************/
// $Id: LogUtil.java,v 1.2 2003/04/12 20:57:16 hyoon Exp $


package com.bluecraft.webclient.boilerplate;

import com.bluecraft.webclient.*;

import com.bluecraft.webclient.config.*;
import com.bluecraft.webclient.resource.*;
import com.bluecraft.webclient.logging.*;
import com.bluecraft.webclient.util.*;

import java.util.logging.Logger;


/**   
 * Utility class for Java logging framework.
 *
 * @author   <A href="mailto:hyoon@bluecraft.com">Hyoungsoo Yoon</A>
 * @version  $Revision: 1.2 $
 */
final class LogUtil
    extends AbstractLogUtil
{
    private static final String sName = "com.bluecraft.webclient.boilerplate";
    
    private LogUtil()
    {
    }

    static Logger getLogger()
    {
        return Logger.getLogger(sName,getResourceBundle());
    }
  
}


