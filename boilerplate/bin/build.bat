@echo off

REM ----------------------------------------------------------
REM WebClient Boilerplate App Main Build Script.
REM This script is tested under Windows NT/2K & WinXP.
REM ----------------------------------------------------------

setlocal

REM -- Root directory for the project
set _PROJECTDIR=%PROJECTDIR%
set PROJECTDIR=E:/webclient/boilerplate

REM -- Name of the build file to use
set _MAINBUILDFILE=%MAINBUILDFILE%
set MAINBUILDFILE=%PROJECTDIR%/bin/build.xml


REM --------------------------------------------
REM No need to edit anything past here
REM --------------------------------------------

:init
set _CLASSPATH=%CLASSPATH%
set CLASSPATH=.;..;%CLASSPATH%
REM if exist %PROJECTDIR%/lib/bluecraft.common-0.9.jar set CLASSPATH=%CLASSPATH%;%PROJECTDIR%/lib/bluecraft.common-0.9.jar
REM if exist %PROJECTDIR%/lib/bluecraft.tools-0.9.jar set CLASSPATH=%CLASSPATH%;%PROJECTDIR%/lib/bluecraft.tools-0.9.jar
for %%l IN (%PROJECTDIR%\bin\lib\*.jar) DO call %PROJECTDIR%\bin\add2cp %%l


:testjavahome
if "%JAVA_HOME%" == "" goto javahomeerror
goto build

:testanthome
if "%ANT_HOME%" == "" goto anthomeerror
goto build


:build
REM call ant %1
call ant -f %MAINBUILDFILE% %1
REM call ant -Dbasedir=%PROJECTDIR% -f %MAINBUILDFILE% %1
goto end


:javahomeerror
echo ERROR: JAVA_HOME not found in your environment.
echo Please, set the JAVA_HOME variable in your environment to match the
echo location of the Java Virtual Machine you want to use.

:anthomeerror
echo ERROR: ANT_HOME not found in your environment.
echo Please, set the ANT_HOME variable in your environment to match the
echo location of the ant installation.


:end
set CLASSPATH=%_CLASSPATH%
set PROJECTDIR=%_PROJECTDIR%
set MAINBUILDFILE=%_MAINBUILDFILE%

endlocal

